import React from "react";
import PropTypes from "prop-types";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Voucher from "../../vector/voucher.svg";
import { useHistory } from "react-router-dom";

function Component(props) {
  const { classes } = props;
  const history = useHistory();

  return (
    <React.Fragment>
      <div
        className={classes.root}
        onClick={(e) => {
          const isValid = props.click();
          if (isValid) {
            if (e.target.id !== "remove") {
              history.push("/cart-shipment/voucher");
              console.log(e.target.id);
            }
          }
        }}
      >
        <Card
          className={classes.card}
          elevation={0}
          style={{ background: props.cardColor }}
        >
          <Grid container spacing={1} alignItems="center">
            <Grid item xs={1}>
              <img src={Voucher} alt="voucher" />
            </Grid>
            <Grid item xs={7}>
              <Typography className={classes.content}>
                {props.content}
              </Typography>
            </Grid>
            <Grid item xs={4} className={classes.buttonWrapper}>
              {props.remove && (
                <div
                  className={classes.button}
                  style={{ background: props.buttonColor, marginRight: 10 }}
                  id={props.id}
                  onClick={(e) => {
                    localStorage.removeItem("usedVoucher");
                    props.remove();
                  }}
                >
                  {props.remove && (
                    <Typography id="remove" className={classes.buttonText}>
                      Hapus
                    </Typography>
                  )}
                </div>
              )}

              <div
                className={classes.button}
                style={{ background: props.buttonColor }}
              >
                <Typography className={classes.buttonText}>
                  {props.buttonContent}
                </Typography>
              </div>
            </Grid>
          </Grid>
        </Card>
      </div>
    </React.Fragment>
  );
}

Component.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default Component;
