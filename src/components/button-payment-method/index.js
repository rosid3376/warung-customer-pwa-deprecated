import React from "react";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import RightArrow from "@material-ui/icons/KeyboardArrowRightRounded";
import InputAdornment from "@material-ui/core/InputAdornment";
import PaymentIcon from "../../vector/payment-icon.svg";

const ButtonPaymentMethod = ({ onClick, selectedPayment }) => {
  console.log(selectedPayment);
  return (
    <>
      <Typography style={{ marginBottom: 16, fontWeight: 600 }}>
        Metode Pembayaran
      </Typography>
      <TextField
        variant="outlined"
        fullWidth
        disabled
        value={selectedPayment?.name}
        placeholder="Pilih Metode Pembayaran"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <img
                style={{ width: 16, marginRight: 8 }}
                src={PaymentIcon}
                alt="shipment-method"
              />
            </InputAdornment>
          ),
          endAdornment: (
            <InputAdornment position="end">
              <RightArrow
                style={{ color: process.env.REACT_APP_COLOR_PRIMARY }}
              />
            </InputAdornment>
          ),
          style: { cursor: "pointer" },
        }}
        // eslint-disable-next-line
        inputProps={{
          style: {
            cursor: "pointer",
            color: "#333333",
            fontSize: 12,
            fontWeight: 500,
          },
        }}
        onClick={onClick}
      />
    </>
  );
};

export default ButtonPaymentMethod;
