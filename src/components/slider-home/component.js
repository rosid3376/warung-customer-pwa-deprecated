import React from "react";
import { withRouter } from "react-router-dom";
import { Grid, Typography } from "@material-ui/core";
import Carousel from "../../components/carousel";

function Component(props) {
  const { classes } = props;
  return (
    <>
      <Grid container className={classes.container} item xs={12}>
        <Typography style={{ fontSize: 14, fontWeight: "bold" }}>
          Promo Menarik Untukmu!
        </Typography>
        <Typography style={{ fontSize: 10, color: "#52575C" }}>
          Banyak promo produk terbaik untukmu
        </Typography>
        <div style={{ marginTop: 12 }}>
          <Carousel />
        </div>
      </Grid>
    </>
  );
}

export default withRouter(Component);
