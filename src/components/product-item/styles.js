const styles = (theme) => ({
  container: {
    padding: 0,
    paddingTop: 48 + 56,
  },
  card: {
    backgroundColor: '#FFFFFF',
    boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)',
    borderRadius: 10,
    margin: '8px 0',
    padding: 16,
    height: 147,
  },
  media: {
    borderRadius: 8,
    height: 115,
    width: 115,
    '@media (max-width:375px)': {
      width: 85,
    },
    display: 'flex',
  },
  title: {
    color: '#000000',
    fontSize: 12,
    '@media (max-width:375px)': {
      fontSize: 10,
    },
  },
  old: {
    textDecoration: 'line-through',
    color: '#C7C7C9',
  },
  price: {
    color: process.env.REACT_APP_COLOR_PRIMARY,
    fontWeight: 'bold',
    fontSize: 12,
    '@media (max-width:375px)': {
      fontSize: 10,
    },
  },
  cardMediaTitle: {
    color: 'white',
    backgroundColor: '#FF4600',
    width: '50%',
    fontWeight: 'bold',
    fontSize: 10,
    borderRadius: 3,
    opacity: 0.9,
    marginBottom: 4,
    paddingLeft: 4,
    paddingRight: 4,
  },
  cardMediaDisc: {
    backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
    color: process.env.REACT_APP_COLOR_FONT,
    width: 65,
    padding: 10,
    fontWeight: 'bold',
    fontSize: 10,
    borderRadius: '5px 0px 5px 0px',
    opacity: 0.9,
    margin: 0,
  },
  cardMedia: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    margin: 0,
  },
  cardContent: {
    padding: '8px 8px 8px 16px',
  },
  btn: {
    justifyContent: 'center',
    display: 'flex',
    margin: 0,
  },
});

export default styles;
