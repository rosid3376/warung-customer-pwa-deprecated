const styles = (theme) => ({
  card: {
    minWidth: 150,
    height: 300,
    boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.05)',
    borderRadius: 7,
    display: 'flex',
    flexDirection: 'column',
    marginRight: '16px',
    cursor: 'pointer',
  },
  image: {
    height: 150,
    width: '100%',
    borderRadius: '7px 7px 0 0',
    objectFit: 'cover',
  },
  badge: {
    width: '50%',
    height: 30,
    backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
    borderRadius: '5px 0 5px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgeText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: process.env.REACT_APP_COLOR_FONT,
  },
  content: {
    height: 150,
    width: '100%',
    borderRadius: '0 0 7px 7px',
    objectFit: 'cover',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    padding: 16,
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: 11,
  },
  regularPriceText: {
    fontSize: 10,
    color: '#252525',
  },
  salePriceText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: process.env.REACT_APP_COLOR_PRIMARY,
  },
  unitText: {
    fontSize: 9,
    color: '#777777',
  },
  button: {
    height: 30,
    width: '100%',
    backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
    color: process.env.REACT_APP_COLOR_FONT,
    borderRadius: 3,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textTransform: 'capitalize',
    fontSize: '10px !important',
    '&:hover': {
      backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
    },
  },
});

export default styles;
