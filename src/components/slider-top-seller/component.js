/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useContext } from "react";
import { withRouter } from "react-router-dom";
import { Button, ButtonGroup, CardMedia } from "@material-ui/core";
import currencyFormatter from "../../utilities/currency-formatter";
import { CartContext } from "../../context/cart";

function Component(props) {
  const { classes, top } = props;
  const { addCart, increaseCart, decreaseCart, cart, cartUpdated } = useContext(
    CartContext
  );
  const [qty, setQty] = useState(0);

  const discountPercent =
    top.promoPriceType === "PERCENTAGE"
      ? top.promoPrice
      : (100 - (top.promoPrice / top.regularPrice) * 100) % 1 === 0
      ? 100 - (top.promoPrice / top.regularPrice) * 100
      : (100 - (top.promoPrice / top.regularPrice) * 100).toFixed(2);

  useEffect(() => {
    const selectedItem = cart.find((item) => item.id === top.id);
    if (selectedItem) {
      setQty(selectedItem.total);
    } else {
      setQty(0);
    }
  }, [cartUpdated]);

  return (
    <>
      <div className={classes.card}>
        <CardMedia
          image={
            top.image.url ? top.image.url : "https://via.placeholder.com/150"
          }
          className={classes.image}
          onClick={() => {
            props.history.push(`/product/${top.id}?from=/`);
          }}
        >
          {top.isPromo && (
            <div className={classes.badge}>
              <span className={classes.badgeText}>
                Disc. {discountPercent}%
              </span>
            </div>
          )}
        </CardMedia>
        <div className={classes.content}>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              height: "100%",
            }}
            onClick={() => {
              props.history.push(`/product/${top.id}?from=/`);
            }}
          >
            <span className={classes.titleText}>{top.name}</span>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                height: "100%",
              }}
            >
              <span>
                {!top.isPromo ? (
                  <span className={classes.salePriceText}>
                    {currencyFormatter.format(top.regularPrice)}
                  </span>
                ) : (
                  <strike className={classes.regularPriceText}>
                    {currencyFormatter.format(top.regularPrice)}
                  </strike>
                )}
              </span>

              {top.isPromo ? (
                <span className={classes.salePriceText}>
                  {currencyFormatter.format(top.price)}
                </span>
              ) : (
                ""
              )}
            </div>
          </div>
          {qty > 0 ? (
            <ButtonGroup
              size="small"
              aria-label="Small outlined button group"
              style={{
                justifyContent: "flex-end",
                width: "-webkit-fill-available",
                marginRight: 1,
              }}
            >
              <Button
                size="small"
                onClick={(event) => {
                  decreaseCart(top);
                }}
                style={{
                  color: "#153b50",
                  borderRadius: 4,
                  maxWidth: 30,
                  minWidth: 30,
                  maxHeight: 30,
                  minHeight: 30,
                  padding: 1,
                  border: "1px solid silver",
                }}
              >
                -
              </Button>

              <Button
                size="small"
                style={{
                  border: "0",
                  fontWeight: "bold",
                  fontSize: 12,
                  maxWidth: 58,
                  minWidth: 58,
                  maxHeight: 30,
                  minHeight: 30,
                  padding: 1,
                }}
              >
                {qty || 0}
              </Button>
              <Button
                size="small"
                onClick={(event) => {
                  increaseCart(top);
                }}
                style={{
                  color: process.env.REACT_APP_COLOR_FONT,
                  backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
                  border: `1px solid ${process.env.REACT_APP_COLOR_PRIMARY}`,
                  borderRadius: 4,
                  maxWidth: 30,
                  minWidth: 30,
                  maxHeight: 30,
                  minHeight: 30,
                  padding: 1,
                }}
              >
                +
              </Button>
            </ButtonGroup>
          ) : (
            <div>
              {top.onStock ? (
                <Button
                  onClick={(event) => {
                    addCart(top);
                  }}
                  className={classes.button}
                >
                  <b>Tambahkan</b>
                </Button>
              ) : (
                <Button
                  variant="contained"
                  disabled
                  style={{
                    borderRadius: 4,
                    width: "100%",
                    padding: 0,
                    height: 30,
                    fontSize: 8,
                  }}
                >
                  <b>Stok Habis</b>
                </Button>
              )}
            </div>
          )}
        </div>
      </div>
    </>
  );
}

export default withRouter(Component);
