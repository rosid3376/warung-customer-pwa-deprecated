import React, { useState, useContext } from 'react';
import Cart from '../../vector/cartIcon.svg';
import { Box } from '@material-ui/core';
import Badge from '@material-ui/core/Badge';
import { withRouter } from 'react-router-dom';
import Fab from '@material-ui/core/Fab';
import { makeStyles } from '@material-ui/core/styles';
import { CartContext } from '../../context/cart';

function Component(props) {
  const { classes } = props;
  const { cart, price } = useContext(CartContext);
  const item = props.cartItems;
  const [selectedPasar, setSelectedPasar] = useState(
    JSON.parse(localStorage.getItem('selectedPasar'))
  );

  const useStyleku = makeStyles({
    '@global': {
      '.MuiBadge-colorPrimary': {
        backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
        color: process.env.REACT_APP_COLOR_FONT,
        fontWeight: 'bold',
      },
      '.MuiFab-root': {
        boxShadow: '0px -5px 20px rgba(135, 202, 254, 0.16)',
      },
      '.MuiBadge-anchorOriginTopLeftRectangle': {
        top: 7,
        left: 8,
      },
    },
  });

  useStyleku();
  return (
    <React.Fragment>
      {cart.length > 0 && (
        <Box
          align="right"
          position="static"
          style={{
            position: 'fixed',
            bottom: 70,
            paddingRight: 16,
            // width: '100%',
            // maxWidth: 442,
            display: 'flex',
            justifyContent: 'flex-end',
          }}
        >
          <Badge
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
            badgeContent={cart.length}
            color="primary"
          >
            <Fab
              style={{ backgroundColor: '#4B76C2' }}
              onClick={() => {
                props.history.push(props.to);
              }}
            >
              <img src={Cart} />
            </Fab>
          </Badge>
        </Box>
      )}
    </React.Fragment>
  );
}

export default withRouter(Component);
