import React from "react";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import RightArrow from "@material-ui/icons/KeyboardArrowRightRounded";
import InputAdornment from "@material-ui/core/InputAdornment";
import ShipmentMethodIcon from "../../vector/shipment-method-icon.svg";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core";
import currencyFormatter from "../../utilities/currency-formatter";

const useStyles = makeStyles({
  shipmentMethodItemsText: {
    fontSize: 12,
    fontWeight: 500,
  },
});

const ButtonShippingMethod = ({
  selectedShipping,
  openShipmentMethod,
  openCourier,
}) => {
  const classes = useStyles();

  return (
    <>
      <Typography style={{ marginBottom: 16, fontWeight: 600 }}>
        Metode Pengiriman
      </Typography>
      {selectedShipping ? (
        <Box
          style={{ cursor: "pointer" }}
          padding="16px"
          border="1px solid #F5F5F5"
          borderRadius="8px"
          onClick={selectedShipping?.code !== "pickup" && openShipmentMethod}
        >
          {selectedShipping?.code !== "pickup" && (
            <>
              <Box display="flex" justifyContent="space-between">
                <Typography className={classes.shipmentMethodItemsText}>
                  {`${selectedShipping?.serviceName} (${selectedShipping?.estimatedTimeMinimum} - ${selectedShipping?.estimatedTimeMaximum} Hari)`}
                </Typography>
                <RightArrow
                  style={{ color: process.env.REACT_APP_COLOR_PRIMARY }}
                />
              </Box>
              <div
                style={{
                  backgroundColor: "#f5f5f5",
                  margin: "15px 0px",
                  height: "1px",
                }}
              />
            </>
          )}

          <Box display="flex" justifyContent="space-between">
            <Typography className={classes.shipmentMethodItemsText}>
              {`${selectedShipping?.name} (${currencyFormatter.format(
                selectedShipping?.cost
              )})`}
            </Typography>
            <RightArrow
              style={{ color: process.env.REACT_APP_COLOR_PRIMARY }}
            />
          </Box>
        </Box>
      ) : (
        <TextField
          variant="outlined"
          fullWidth
          disabled
          placeholder="Pilih Metode Pengiriman"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <img
                  style={{ width: 16, marginRight: 8 }}
                  src={ShipmentMethodIcon}
                  alt="shipment-method"
                />
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <RightArrow
                  style={{ color: process.env.REACT_APP_COLOR_PRIMARY }}
                />
              </InputAdornment>
            ),
            style: { cursor: "pointer" },
          }}
          // eslint-disable-next-line
          inputProps={{ style: { cursor: "pointer" } }}
          onClick={openShipmentMethod}
        />
      )}
    </>
  );
};

export default ButtonShippingMethod;
