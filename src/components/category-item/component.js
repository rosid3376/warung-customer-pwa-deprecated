import React from 'react';
import Card from '@material-ui/core/Card';
import { withRouter } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';

function Component(props) {
  const { classes, category } = props;
  return (
    <div align="center">
      <Card
        align="center"
        className={classes.card}
        onClick={() => {
          props.history.push(`/category/${category.id}`);
        }}
      >
        <img
          style={{ width: 40 }}
          src={category.image ? category.image.src : ''}
          alt="Items pic"
        />
      </Card>
      <Typography
        style={{ color: '#707585', fontSize: 10 }}
        variant="caption"
        gutterBottom
      >
        {category.name}
      </Typography>
    </div>
  );
}

export default withRouter(Component);
