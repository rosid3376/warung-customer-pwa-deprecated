/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from "react";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import currencyFormatter from "../../utilities/currency-formatter";
import { withRouter } from "react-router-dom";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { CartContext } from "../../context/cart";

function TopSeller(props) {
  const { classes, top } = props;
  const { addCart, increaseCart, decreaseCart, cart, cartUpdated } = useContext(
    CartContext
  );
  const [qty, setQty] = useState(0);

  useEffect(() => {
    const selectedItem = cart.find((item) => item.id === top.id);
    if (selectedItem) {
      setQty(selectedItem.total);
    } else {
      setQty(0);
    }
  }, [cartUpdated]);

  const discountPercent =
    top.promoPriceType === "PERCENTAGE"
      ? top.promoPrice
      : (100 - (top.promoPrice / top.regularPrice) * 100) % 1 === 0
      ? 100 - (top.promoPrice / top.regularPrice) * 100
      : (100 - (top.promoPrice / top.regularPrice) * 100).toFixed(2);

  return (
    <Box className={classes.card}>
      <Grid container spacing={0}>
        <Grid
          onClick={() => {
            props.topSeller
              ? props.history.push(`/product/${top.id}?from=/top-seller`)
              : props.history.push(
                  `/product/${top.id}?from=/category/${top.category_id}`
                );
          }}
          item
          xs={3}
        >
          <CardMedia
            className={classes.media}
            image={
              top.image.url ? top.image.url : "https://via.placeholder.com/150"
            }
          >
            <div className={classes.cardMedia}>
              {top.isPromo && (
                <p className={classes.cardMediaDisc}>
                  Disc.
                  {discountPercent}%
                </p>
              )}
            </div>
          </CardMedia>
        </Grid>
        <Grid
          onClick={() => {
            props.topSeller
              ? props.history.push(`/product/${top.id}?from=/top-seller`)
              : props.history.push(
                  `/product/${top.id}?from=/category/${top.category_id}`
                );
          }}
          item
          xs={6}
        >
          <CardContent>
            <Typography style={{ marginBottom: 10 }} className={classes.title}>
              {top.name}
            </Typography>
            {top.isPromo && (
              <Typography variant="caption" className={classes.old}>
                <b>{currencyFormatter.format(top.regularPrice)}</b>
              </Typography>
            )}
            {top.isPromo && (
              <Typography className={classes.price}>
                {currencyFormatter.format(top.price)}
                <b style={{ color: "#C7C7C9", fontSize: 10 }}>
                  {" "}
                  <b style={{ color: "#C7C7C9", fontSize: 10 }}>
                    {top.unit ? "/" + top.unit : ""}
                  </b>
                </b>
              </Typography>
            )}
            {!top.isPromo && (
              <Typography className={classes.price} style={{ marginTop: 30 }}>
                {currencyFormatter.format(top.price)}
                <b style={{ color: "#C7C7C9", fontSize: 10 }}>
                  {" "}
                  <b style={{ color: "#C7C7C9", fontSize: 10 }}>
                    {top.unit ? "/" + top.unit : ""}
                  </b>
                </b>
              </Typography>
            )}
          </CardContent>
        </Grid>
        <Grid align="right" item xs={3}>
          <div
            style={{ minHeight: 80, minWidth: "100%" }}
            onClick={() => {
              props.topSeller
                ? props.history.push(`/product/${top.id}?from=/top-seller`)
                : props.history.push(
                    `/product/${top.id}?from=/category/${top.category_id}`
                  );
            }}
          ></div>
          <div>
            {qty > 0 ? (
              <ButtonGroup
                size="small"
                aria-label="Small outlined button group"
                style={{
                  justifyContent: "flex-end",
                  width: "-webkit-fill-available",
                  marginRight: 1,
                }}
              >
                <Button
                  size="small"
                  onClick={(event) => {
                    decreaseCart(top);
                  }}
                  style={{
                    color: "#153b50",
                    borderRadius: 4,
                    maxWidth: 30,
                    minWidth: 30,
                    maxHeight: 30,
                    minHeight: 30,
                    padding: 1,
                    border: "1px solid silver",
                  }}
                >
                  -
                </Button>

                <Button
                  size="small"
                  style={{
                    border: "0",
                    fontWeight: "bold",
                    fontSize: 12,
                    maxWidth: 30,
                    minWidth: 30,
                    maxHeight: 30,
                    minHeight: 30,
                    padding: 1,
                  }}
                >
                  {qty || 0}
                </Button>
                <Button
                  size="small"
                  onClick={(event) => {
                    increaseCart(top);
                  }}
                  style={{
                    color: process.env.REACT_APP_COLOR_FONT,
                    backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
                    border: "1px solid process.env.REACT_APP_COLOR_SECONDARY",
                    borderRadius: 4,
                    maxWidth: 30,
                    minWidth: 30,
                    maxHeight: 30,
                    minHeight: 30,
                    padding: 1,
                  }}
                >
                  +
                </Button>
              </ButtonGroup>
            ) : (
              <div>
                {top.onStock ? (
                  <Button
                    onClick={(event) => {
                      addCart(top);
                    }}
                    style={{
                      color: process.env.REACT_APP_COLOR_FONT,
                      backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
                      borderRadius: 4,
                      width: 90,
                      height: 30,
                      fontSize: 10,
                      textTransform: "none",
                    }}
                  >
                    <b>Tambahkan</b>
                  </Button>
                ) : (
                  <Button
                    variant="contained"
                    disabled
                    style={{
                      borderRadius: 4,
                      width: 90,
                      padding: 0,
                      height: 30,
                      fontSize: 8,
                    }}
                  >
                    <b>Stok Habis</b>
                  </Button>
                )}
              </div>
            )}
          </div>
        </Grid>
      </Grid>
    </Box>
  );
}

export default withRouter(TopSeller);
