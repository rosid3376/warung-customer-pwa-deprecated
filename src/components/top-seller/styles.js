const styles = (theme) => ({
  card: {
    borderRadius: 8,
    padding: 16,
  },
  media: {
    height: '100%',
    borderRadius: 8,
  },
  title: {
    color: '#000000',
    fontSize: 12,
  },
  cardMedia: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    height: '-webkit-fill-available',
  },
  old: {
    textDecoration: 'line-through',
    color: '#C7C7C9',
  },
  price: {
    color: process.env.REACT_APP_COLOR_PRIMARY,
    fontWeight: 'bold',
    fontSize: 12,
  },
  cardMediaTitle: {
    color: 'white',
    backgroundColor: '#FF4600',
    width: '20%',
    fontWeight: 'bold',
    fontSize: 10,
    borderRadius: 3,
    opacity: 0.9,
    marginBottom: 4,
    paddingLeft: 4,
    paddingRight: 4,
  },
  cardMediaDisc: {
    backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
    color: process.env.REACT_APP_COLOR_FONT,
    width: 'fit-content',
    padding: '8px 12px',
    fontWeight: 'bold',
    fontSize: 10,
    borderRadius: '5px 0 5px 0',
    opacity: 0.9,
    marginTop: 0,
    marginBottom: 46,
    paddingLeft: 4,
    paddingRight: 4,
  },
});

export default styles;
