import React, { useEffect, useState } from 'react';
import AppBar from '../../components/app-bar/component';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import firebase from 'firebase/app';
import 'firebase/auth';
import Divider from '@material-ui/core/Divider';
import info from '../../vector/info.svg';
import Fab from '../../components/wa-fab';

function Component(props) {
  const [state, setState] = useState({
    data: {},
  });
  useEffect(() => {
    const data = firebase.auth().currentUser;
    setState({ data });
  }, []);

  const { classes } = props;
  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <CssBaseline />
      <AppBar title="Pusat Bantuan" />
      <Grid
        elevation={0}
        align="center"
        style={{
          background: process.env.REACT_APP_COLOR_PRIMARY,
          borderRadius: 0,
          color: process.env.REACT_APP_COLOR_FONT,
          opacity: 0.7,
          paddingTop: 20,
          paddingBottom: 20,
          // boxShadow:
          //   '0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)',
        }}
      >
        <Typography variant="subtitle1" gutterBottom>
          <b>
            Hello &nbsp;
            {state.data !== null ? state.data.displayName : 'User'}
          </b>
        </Typography>

        <Typography variant="h6" gutterBottom>
          <b> Anda Memerlukan Bantuan?</b>
        </Typography>
      </Grid>

      <Grid container spacing={0} className={classes.gridList}>
        <Grid item xs={12}>
          <Typography className={classes.textLixt}>
            <b>Tentang Kami</b>
          </Typography>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/apa-itu`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Apa itu {process.env.REACT_APP_BRAND_NAME}?
            </Typography>
          </Link>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/dari-mana`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Produk {process.env.REACT_APP_BRAND_NAME} berasal dari mana?
            </Typography>
          </Link>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/standar-produksi`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Apa standar produksi para petani{' '}
              {process.env.REACT_APP_BRAND_NAME}?
            </Typography>
          </Link>
        </Grid>
      </Grid>

      <Grid container spacing={0} className={classes.gridListTwo}>
        <Grid item xs={12}>
          <Typography className={classes.textLixt}>
            <b>Akun</b>
          </Typography>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/cara-login`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Bagaimana cara login akun di {process.env.REACT_APP_BRAND_NAME}?
            </Typography>
          </Link>
          {/* <Divider style={{ marginTop: '4%' }} /> */}
        </Grid>
        {/* <Grid item xs={12} className={classes.what}>
          <Link to={`help/lupa-sandi`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Bagaimana kalau lupa kata sandi?
            </Typography>
          </Link>
        </Grid> */}
      </Grid>

      <Grid container spacing={0} className={classes.gridListTwo}>
        <Grid item xs={12}>
          <Typography className={classes.textLixt}>
            <b>Operasional</b>
          </Typography>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/kota-operasional`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Operasional {process.env.REACT_APP_BRAND_NAME} sudah ada di kota
              mana saja?
            </Typography>
          </Link>
        </Grid>
      </Grid>

      <Grid container spacing={0} className={classes.gridListTwo}>
        <Grid item xs={12}>
          <Typography className={classes.textLixt}>
            <b>Harga dan Transaksi</b>
          </Typography>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/ongkos-kirim`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Berapa ongkos kirimnya?
            </Typography>
          </Link>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/menerima-kembalian`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Bagaimana saya dapat kembalian uang?
            </Typography>
          </Link>
        </Grid>
      </Grid>

      <Grid container spacing={0} className={classes.gridListTwo}>
        <Grid item xs={12}>
          <Typography className={classes.textLixt}>
            <b>Pesanan</b>
          </Typography>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/seberapa-cepat`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Seberapa cepat {process.env.REACT_APP_BRAND_NAME} mengantarkan
              pesanan?
            </Typography>
          </Link>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/waktu-pengantaran`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Apa yang dimaksud dengan waktu pengantaran?
            </Typography>
          </Link>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/cek-status-pesanan`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Bagaimana saya mengecek status pesanan saya?
            </Typography>
          </Link>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link
            to={`help/merubah-membatalkan-pesanan`}
            className={classes.link}
          >
            <Typography className={classes.textLixt}>
              Bagaimana cara merubah atau membatalkan pesanan?
            </Typography>
          </Link>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/pembatalan-pesanan`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Apa kebijakan terkait pembatalan pesanan?
            </Typography>
          </Link>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/ganti-rugi`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Bagaimana Proses Ganti Rugi Jika Barang Rusak/Hilang Dalam
              Pengiriman?
            </Typography>
          </Link>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Link to={`help/lebih-lanjut`} className={classes.link}>
            <Typography className={classes.textLixt}>
              Saya punya pertanyaan lebih lanjut untuk{' '}
              {process.env.REACT_APP_BRAND_NAME}!
            </Typography>
          </Link>
          <Divider style={{ marginTop: '4%' }} />
        </Grid>

        <Grid item xs={12} className={classes.what}>
          <Typography className={classes.textLixt}>
            Masih <b>butuh bantuan</b> atau <b>punya pertanyaan lain</b>
            &nbsp; yang ingin ditanyakan?
            <a
              target="_blank"
              rel="noreferrer"
              href={`https://api.whatsapp.com/send?phone=${process.env.REACT_APP_WHATSAPP_NUMBER}&text=Hai ${process.env.REACT_APP_BRAND_NAME}, mau tanya dong`}
              style={{
                marginLeft: 6,
                color: process.env.REACT_APP_COLOR_PRIMARY,
                textDecoration: 'none',
              }}
            >
              <b>HUBUNGI KAMI</b>
            </a>
          </Typography>
        </Grid>
      </Grid>

      <Grid style={{ padding: 10 }} container spacing={0}>
        <Grid
          item
          xs={1}
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '-5%',
          }}
        >
          <img src={info} alt="Info" />
        </Grid>
        <Grid item xs={11}>
          <Typography
            variant="caption"
            display="block"
            gutterBottom
            style={{ color: '#898B8C', fontWeight: 'bold' }}
          >
            Layanan Pelanggan 24 Jam, Senin s/d Minggu, tidak termasuk Hari
            Libur Nasional.
          </Typography>
        </Grid>
      </Grid>
      <div style={{ marginLeft: '74%' }}>
        <a
          target="_blank"
          rel="noreferrer"
          href={`https://api.whatsapp.com/send?phone=${process.env.REACT_APP_WHATSAPP_NUMBER}&text=Hai ${process.env.REACT_APP_BRAND_NAME}, mau tanya dong`}
          style={{
            color: process.env.REACT_APP_COLOR_FONT,
            textDecoration: 'none',
          }}
        >
          <Fab />
        </a>
      </div>
    </Container>
  );
}

export default Component;
