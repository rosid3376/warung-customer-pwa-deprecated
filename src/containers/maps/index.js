import React, { useState, useEffect, useCallback } from "react";
import {
  Container,
  makeStyles,
  Button,
  Typography,
  Divider,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";

import {
  getLocationByOpenStreetMapReverse,
  getLocationByOpenStreetMapStreet,
} from "../../services/address";

import { useHistory, useLocation } from "react-router";
import MapLeaflet from "./../../components/map-leaflet";
import {
  MapOutlined,
  ArrowBackIos as ArrowBackIcon,
  NavigationOutlined,
} from "@material-ui/icons";
import Skeleton from "@material-ui/lab/Skeleton";
import LocationOnIcon from "@material-ui/icons/LocationOn";

const styles = makeStyles((theme) => ({
  root: {
    paddingLeft: 0,
    paddingRight: 0,
    backgroundColor: "white",
    minHeight: "100vh",
    maxWidth: 444,
    width: "100%",
  },
  backButton: {
    boxShadow: "0px 0px 7px -2px rgba(50, 50, 50, 1)",
    width: "fit-content",
    borderRadius: 10,
    padding: 12,
    display: "flex",
    alignItems: "center",
    position: "fixed",
    top: 0,
    marginLeft: 16,
    marginTop: 16,
    backgroundColor: "#fff",
    zIndex: 9999,
    cursor: "pointer",
  },
  nope: {
    height: 5,
    width: 100,
    backgroundColor: "#e4e4e4",
    borderRadius: 30,
    margin: "20px auto",
    marginTop: 0,
    cursor: "pointer",
  },
  detailWrapper: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    position: "fixed",
    padding: 16,
    maxWidth: 444,
    bottom: 0,
    backgroundColor: "#fff",
  },
  navigatorIconWrapper: {
    border: `1px solid ${theme.palette.primary.main}`,
    width: "fit-content",
    padding: 6,
    display: "flex",
    borderRadius: "50%",
  },
  mapnavicon: {
    position: "fixed",
    zIndex: 9999,
    top: 0,
    color: process.env.REACT_APP_COLOR_PRIMARY,
    width: 40,
    height: 40,
  },
}));

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function Maps() {
  const classes = styles();
  const theme = useTheme();
  const query = useQuery();
  const history = useHistory();
  const id = query.get("id");

  const address = JSON.parse(localStorage.getItem("address"));
  const [coordinates, setCoordinates] = useState([-7.0, 110.4305923]);
  const Laptop = useMediaQuery("(min-width:992px)");
  const [marginIcon, setMarginIcon] = useState({
    left: 0,
    top: 0,
  });
  const [curretLocation, setCurrentLocation] = useState({ display_name: "" });
  const [isLoading, setIsLoading] = useState(true);
  const lat = query.get("lat");
  const long = query.get("long");
  useEffect(() => {
    setMarginIcon({
      left: window.screen.width / 2 - 20,
      top: (window.screen.height - 185) / 2 - 20,
    });

    setCoordinates([lat, long]);
  }, []);

  console.log(coordinates);
  console.log(Laptop);
  useEffect(() => {
    setIsLoading(true);
    const fetch = async () => {
      const response = await getLocationByOpenStreetMapReverse(
        coordinates[0],
        coordinates[1]
      );
      if (response) {
        setIsLoading(false);
        setCurrentLocation(response);
      }
    };

    fetch();
  }, [coordinates]);

  const handleChangeCoordinate = (latitude, longitude) => {
    setCoordinates([latitude, longitude]);
  };

  return (
    <Container maxWidth="xs" className={classes.root}>
      <div
        className={classes.backButton}
        onClick={() =>
          id
            ? history.push(`/new-address/detail?id=${id}`)
            : history.push("/new-address/detail")
        }
      >
        <ArrowBackIcon style={{ color: theme.palette.primary.main }} />
      </div>

      <div style={{ height: "100%", paddingBottom: 200 }}>
        <MapLeaflet
          center={[lat, long]}
          handleChangeCoordinate={handleChangeCoordinate}
        />
        <LocationOnIcon
          className={classes.mapnavicon}
          style={{
            marginLeft: Laptop ? 202 : "45%",
            marginTop: Laptop ? marginIcon.top : "45%",
          }}
        />
      </div>

      {!isLoading ? (
        <div className={classes.detailWrapper}>
          <div className={classes.nope} />
          <Typography style={{ fontWeight: "bold" }}>
            Tetapkan Titik Lokasi
          </Typography>
          <div style={{ display: "flex", alignItems: "center", marginTop: 8 }}>
            <div className={classes.navigatorIconWrapper}>
              <NavigationOutlined
                style={{ color: theme.palette.primary.main }}
              />
            </div>
            <Typography variant="caption" style={{ marginLeft: 16 }}>
              {curretLocation.display_name}
            </Typography>
          </div>

          <Button
            variant="contained"
            color="primary"
            fullWidth
            size="large"
            style={{ marginTop: 16 }}
            onClick={() =>
              address
                ? history.push(
                    `/cart-shipment/update-address?lat=${coordinates[0]}&long=${coordinates[1]}`
                  )
                : id
                ? history.push(
                    `/cart-shipment/update-address?id=${id}&lat=${coordinates[0]}&long=${coordinates[1]}`
                  )
                : history.push(
                    `/new-address?lat=${coordinates[0]}&long=${coordinates[1]}`
                  )
            }
          >
            Set Lokasi
          </Button>
        </div>
      ) : (
        <div className={classes.detailWrapper}>
          <div className={classes.nope} />
          <Skeleton variant="text" width={150} />
          <div style={{ display: "flex", alignItems: "center", marginTop: 8 }}>
            <div
              className={classes.navigatorIconWrapper}
              style={{ border: "none" }}
            >
              <Skeleton variant="circle" height={40} width={40} />
            </div>
            <div style={{ width: "100%" }}>
              <Skeleton variant="text" width="100%" />
              <Skeleton variant="text" width="100%" />
              <Skeleton variant="text" width="50%" />
            </div>
          </div>

          <Skeleton variant="text" width="100%" height={42} />
        </div>
      )}
    </Container>
  );
}

export default Maps;
