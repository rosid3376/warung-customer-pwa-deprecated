import React, { useState, useEffect } from "react";
import {
  Container,
  makeStyles,
  CssBaseline,
  TextField,
  Paper,
  withStyles,
  Typography,
  InputAdornment,
  InputBase,
  Button,
  Dialog,
  Divider,
  AppBar,
  Toolbar,
} from "@material-ui/core";
// import AppBar from "../../components/app-bar";
import Skeleton from "@material-ui/lab/Skeleton";
import ReactFlagsSelect from "react-flags-select";
import CountryData from "../../utilities/country-code";
import BackButton from "@material-ui/icons/ArrowBackIos";
import ArrowDown from "@material-ui/icons/KeyboardArrowDown";
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";
import {
  getProvinces,
  getCities,
  searchProvince,
  searchCity,
  createAddress,
  getLocationByOpenStreetMapReverse,
  getUserAddrres,
} from "../../services/address";
import { useHistory, useLocation, useParams } from "react-router-dom";
import { queryString } from "query-string";

const useStyles = makeStyles({
  container: {
    padding: 0,
    paddingTop: 64,
    backgroundColor: "#FAFAFA",
    borderLeft: "1px solid #f1f1f1",
    borderRight: "1px solid #f1f1f1",
    paddingBottom: 64,
  },
  appBar: {
    left: "auto",
    right: "auto",
  },
  searchDiv: {
    height: 45,
    borderRadius: 100,
    backgroundColor: "#FAFAFA",
    borderRadius: 8,
    padding: "12px 16px",
    display: "flex",
    alignItems: "center",
    margin: "0px 16px 16px",
  },
  searchIcon: {
    marginRight: 10,
  },
  inputRoot: {
    marginLeft: 0,
    color: "inherit",
    width: "100%",
    height: "100%",
  },
  inputInput: {
    width: "100%",
    fontSize: "14px !important",
  },

  flagButton: {
    border: "unset",
    padding: "5px 0px",
    "&:focus": {
      outline: "unset",
    },
  },
  navButton: {
    position: "fixed",
    bottom: 0,
    width: "100%",
    maxWidth: 444,
    padding: 16,
    backgroundColor: "white",
    borderLeft: "1px solid #f1f1f1",
    borderRight: "1px solid #f1f1f1",
  },
  button: {
    textTransform: "capitalize",
    width: "100%",
    "&:hover": {
      backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
    },
  },
  noBorder: {
    border: "none",
  },
  input: {
    padding: 14,
  },
  paperFullScreen: {
    maxWidth: 444,
    height: "75vh",
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  card: {
    display: "flex",
    alignItems: "flex-end",
  },

  list: {
    margin: "70px 0px 0px",
    overflow: "auto",
  },
});

const StyledTextField = withStyles({
  root: {
    "& input": {
      backgroundColor: "#FAFAFA",
      borderRadius: 8,
      padding: "12px 16px",
    },
    "& input::placeholder": {
      fontWeight: 400,
      fontSize: 14,
      color: "#808080",
    },
    "& fieldset": {
      border: "unset",
    },
  },
})(TextField);

const CustomTextField = ({ label, placeholder }) => (
  <div style={{ marginBottom: 24, padding: "0px 16px" }}>
    <Typography style={{ fontWeight: 500, fontSize: 12, marginBottom: 8 }}>
      {label}
    </Typography>
    <StyledTextField
      fullWidth
      placeholder={placeholder}
      id="outlined-basic"
      variant="outlined"
    />
  </div>
);

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const AddAddress = () => {
  const classes = useStyles();
  const history = useHistory();
  const params = useParams();
  const query = useQuery();
  const lat = query.get("lat");
  const long = query.get("long");
  const [selectedCountryCode, setSelectedCountryCode] = useState("ID");
  const [province, setProvince] = useState([]);
  const [city, setCity] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [searchProvinceData, setSearchProvinceData] = useState("");
  const [searchCityData, setSearchCityData] = useState("");
  const [selectedCity, setSelectedCity] = useState("");
  const [selectedProvince, setSelectedProvince] = useState("");
  const [provinceId, setProvinceId] = useState("");
  const user = JSON.parse(localStorage.getItem("users"));
  const [isLoading, setIsLoading] = useState(false);
  const [address, setAddress] = useState([]);
  const [currentLocation, setCurrentLocation] = useState(null);
  const [data, setData] = useState({
    name: "",
    email: "",
    phone: "",
    label: "",
    provinceId: "",
    cityId: "",
    postcode: "",
    address: "",
    latitude: "",
    longitude: "",
    country: "indonesia",
  });
  const [phoneCode, setPhoneCode] = useState({
    country_code: "ID",
    name: "Indonesia",
    dial_code: "+62",
  });
  console.log(selectedProvince);

  const handleClickOpen = (key) => {
    setOpen(key);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleChangeProvice = (e) => {
    setSearchProvinceData(e.target.value);
    setIsLoading(true);
  };
  const handleChangeCity = (e) => {
    setSearchCityData(e.target.value);
    setIsLoading(true);
  };
  console.log(data);

  const handleSave = async () => {
    setIsLoading(true);

    const listAddress = await getUserAddrres();
    console.log(listAddress?.data);
    setAddress(listAddress?.data);

    const response = await createAddress({
      ...data,
      phone: (phoneCode.dial_code + data.phone).replace("+", ""),
      default: address.length < 1 ? true : false,
    });
    console.log(response);
    if (response?.meta?.statusCode === 200 && address.length > 0) {
      setIsLoading(false);
      history.push("/cart-shipment/address");
      localStorage.removeItem("temp_data_address");
    } else {
      setIsLoading(false);
      history.push("/cart-shipment?tabs=1");
      localStorage.removeItem("temp_data_address");
    }
  };
  const handleSaveGuest = () => {
    localStorage.setItem("address", JSON.stringify(data));
    localStorage.removeItem("temp_data_address");
    history.push("/cart-shipment?tabs=1");
  };

  const saveAddress = () => {
    if (user) {
      handleSave();
    } else {
      handleSaveGuest();
    }
  };

  const multipleData = (newData) => {
    setData({
      ...data,
      ...newData,
    });
  };

  const changeData = (key, value) => {
    setData({ ...data, [key]: value });
    console.log({ ...data, [key]: value });
  };

  useEffect(() => {
    async function fetchAPI() {
      const res = await getProvinces();

      setProvince(res.data);
    }
    fetchAPI();
    getCoordinates();
  }, []);

  useEffect(() => {
    const cityAPI = async () => {
      const response = await getCities(provinceId);
      setCity(response.data);
    };
    cityAPI();
  }, [selectedProvince]);

  const getCoordinates = () => {
    const lat = query.get("lat");
    const long = query.get("long");

    if (lat && long) {
      // console.log({ ...data, latitude: lat, longitude: long });
      console.log(selectedProvince);
      const localData = JSON.parse(localStorage.getItem("temp_data_address"));
      if (user) {
        setData({
          ...localData,
          latitude: lat,
          longitude: long,
          country: "indonesia",
        });
        localStorage.setItem(
          "temp_data_address",
          JSON.stringify({
            country: "indonesia",
            name: "",
            email: "",
            phone: "",
            label: "",
            provinceId: "",
            cityId: "",
            postcode: "",
            address: "",
            ...data,
            latitude: lat,
            longitude: long,
          })
        );
      } else {
        setData({
          ...localData,
          latitude: lat,
          longitude: long,
          country: "indonesia",
        });
        localStorage.setItem(
          "temp_data_address",
          JSON.stringify({
            country: "indonesia",
            name: "",
            email: "",
            phone: "",
            label: "",
            provinceId: "",
            cityId: "",
            postcode: "",
            address: "",
            ...data,
            latitude: lat,
            longitude: long,
            province: "",
            city: "",
          })
        );
      }
    } else {
      const localData = JSON.parse(localStorage.getItem("temp_data_address"));
      console.log(localData);
      console.log(selectedProvince);

      if (user) {
        setData({
          latitude: 0,
          longitude: 0,
          country: "indonesia",
          name: "",
          email: "",
          phone: "",
          label: "",
          provinceId: "",
          cityId: "",
          postcode: "",
          address: "",

          ...localData,
        });
        localStorage.setItem(
          "temp_data_address",
          JSON.stringify({
            latitude: 0,
            longitude: 0,
            country: "indonesia",
            name: "",
            email: "",
            phone: "",
            label: "",
            provinceId: "",
            cityId: "",
            postcode: "",
            address: "",

            ...localData,
          })
        );
      } else {
        setData({
          latitude: 0,
          longitude: 0,
          country: "indonesia",
          name: "",
          email: "",
          phone: "",
          label: "",
          provinceId: "",
          cityId: "",
          postcode: "",
          address: "",
          province: "",
          city: "",
          ...localData,
        });
        localStorage.setItem(
          "temp_data_address",
          JSON.stringify({
            latitude: 0,
            longitude: 0,
            country: "indonesia",
            name: "",
            email: "",
            phone: "",
            label: "",
            provinceId: "",
            cityId: "",
            postcode: "",
            address: "",
            province: "",
            city: "",
            ...localData,
          })
        );
      }
    }
  };

  const validateSave = () => {
    if (
      data.name?.length < 3 ||
      data.email?.length < 3 ||
      data.phone?.length < 5 ||
      data.label?.length < 1 ||
      data.provinceId?.length <= 1 ||
      data.cityId?.length <= 1 ||
      data.postcode?.length < 2 ||
      data.address?.length < 3 ||
      data?.latitude === "" ||
      data?.longitude === ""
    ) {
      return false;
    } else {
      return true;
    }
  };

  useEffect(() => {
    const searchAPI = async () => {
      const res = await searchProvince(searchProvinceData);
      const response = await searchCity(searchCityData);
      setProvince(res.data);
      setCity(response.data);
      if (res.meta.statusCode === 200 || response.meta.statusCode === 200) {
        setIsLoading(false);
      }
    };
    const timer = setTimeout(() => {
      searchAPI();
    }, 1000);
    return () => clearTimeout(timer);
  }, [searchCityData, searchProvinceData]);

  useEffect(() => {
    localStorage.setItem("temp_data_address", JSON.stringify(data));
  }, [JSON.stringify(data)]);

  useEffect(() => {
    const fetch = async () => {
      const response = await getLocationByOpenStreetMapReverse(lat, long);
      if (response) {
        setCurrentLocation(response);
      }
    };
    if (lat && long) {
      fetch();
    }
  }, []);

  useEffect(() => {
    const data = JSON.parse(localStorage.getItem("temp_data_address"));
    const localSelectedProvince = province.find(
      (item) => item?.id === Number(data?.provinceId)
    );
    console.log(data);
    const localSelectedCity = city.find(
      (item) => item?.id === Number(data?.cityId)
    );

    if (localSelectedProvince && !selectedProvince) {
      setSelectedProvince(localSelectedProvince.name);
    }
    console.log(selectedProvince);

    if (localSelectedCity && !selectedCity) {
      setSelectedCity(localSelectedCity.name);
    }
  }, [JSON.stringify(province), JSON.stringify(city)]);

  console.log(isLoading);
  console.log(address);
  const back = () => {
    address.length < 1
      ? history.push("/cart-shipment?tabs=1")
      : user
      ? history.push("/cart-shipment/address")
      : history.push("/cart-shipment?tabs=1");
  };
  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <CssBaseline />
      <Dialog
        open={open}
        fullScreen
        onClose={handleClose}
        classes={{
          paperFullScreen: classes.paperFullScreen,
          container: classes.card,
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: "8px 0px 16px",
          }}
        >
          <div
            style={{
              backgroundColor: "#f5f5f5",
              height: 4,
              width: "20%",
              borderRadius: 5,
            }}
          ></div>
        </div>

        {open === "provinsi" && (
          <>
            <div
              style={{
                position: "fixed",
                width: "100%",
                maxWidth: 444,
                padding: "25px 0px 0px",
                backgroundColor: "white",
                borderRadius: 8,
              }}
            >
              <div className={classes.searchDiv}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  placeholder="Cari Provinsi"
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                  }}
                  InputProps={{
                    "aria-label": "search",
                  }}
                  onChange={handleChangeProvice}
                  value={searchProvinceData}
                />
                {searchProvinceData.length > 0 && (
                  <CloseIcon
                    onClick={() => setSearchProvinceData("")}
                    style={{ cursor: "pointer" }}
                  />
                )}
              </div>
            </div>
            <div className={classes.list} onClick={handleClose}>
              {isLoading ? (
                <div style={{ padding: "0px 16px" }}>
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                </div>
              ) : (
                <>
                  {province.map((province) => (
                    <div
                      onClick={() => {
                        setSelectedProvince(province.name);
                        setProvinceId(province.id);

                        user
                          ? changeData("provinceId", province.id.toString())
                          : multipleData({
                              provinceId: province.id.toString(),
                              province: province.name,
                            });
                      }}
                    >
                      <div style={{ padding: 16 }}>{province.name}</div>
                      <Divider />
                    </div>
                  ))}
                </>
              )}
            </div>
            {province.length < 1 && (
              <p style={{ textAlign: "center" }}>Provinsi tidak ditemukan</p>
            )}
          </>
        )}

        {open === "kota" && (
          <>
            <div
              style={{
                position: "fixed",
                width: "100%",
                maxWidth: 444,
                padding: "25px 0px 0px",
                backgroundColor: "white",
                borderRadius: 8,
              }}
            >
              <div className={classes.searchDiv}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  placeholder="Cari Kota/Kabupaten"
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                  }}
                  InputProps={{
                    "aria-label": "search",
                  }}
                  onChange={handleChangeCity}
                  value={searchCityData}
                />
                {searchCityData.length > 0 && (
                  <CloseIcon
                    onClick={() => setSearchCityData("")}
                    style={{ cursor: "pointer" }}
                  />
                )}
              </div>
            </div>
            <div className={classes.list} onClick={handleClose}>
              {isLoading ? (
                <div style={{ padding: "0px 16px" }}>
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                </div>
              ) : (
                <>
                  {" "}
                  {city.map((city) => (
                    <div
                      onClick={(e) => {
                        setSelectedCity(city.name);
                        user
                          ? changeData("cityId", city.id.toString())
                          : multipleData({
                              cityId: city.id.toString(),
                              city: city.name,
                            });
                      }}
                    >
                      <div style={{ padding: 16 }}>{city.name}</div>
                      <Divider />
                    </div>
                  ))}
                </>
              )}
            </div>
            {city.length < 1 && (
              <p style={{ textAlign: "center" }}>Kota tidak ditemukan</p>
            )}
          </>
        )}
      </Dialog>
      {/* <AppBar title="Tambah Alamat" goBack={true} /> */}
      <AppBar
        style={{ width: "100%", maxWidth: 444, backgroundColor: "white" }}
        classes={{ positionFixed: classes.appBar }}
        elevation={1}
      >
        <Toolbar>
          <div style={{ display: "flex", alignItems: "center" }}>
            <BackButton
              style={{
                color: process.env.REACT_APP_COLOR_PRIMARY,
                cursor: "pointer",
                marginRight: 10,
              }}
              onClick={() => back()}
            />

            <strong>Tambah Alamat</strong>
          </div>
        </Toolbar>
      </AppBar>
      <Paper style={{ padding: "16px 0px 25px" }}>
        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Nama Penerima
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            type="text"
            required
            placeholder="Masukkan Nama Lengkap"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("name", e.target.value)}
            value={data.name}
          />
        </div>
        {data.name.length < 3 && (
          <div style={{ margin: "10px 25px", color: "red", fontSize: 11 }}>
            Isi bidang setidaknya 3 karakter
          </div>
        )}
        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Email
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            id="email"
            type="email"
            required
            placeholder="Masukkan Email Aktif"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("email", e.target.value)}
            value={data.email}
          />
        </div>
        {data.email.length < 6 && (
          <div style={{ margin: "10px 25px", color: "red", fontSize: 11 }}>
            Email tidak valid
          </div>
        )}
        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Nomor Telepon
        </div>
        <div className={classes.searchDiv}>
          <div className={classes.searchIcon}>
            <ReactFlagsSelect
              selected={selectedCountryCode}
              showSelectedLabel={false}
              fullWidth={false}
              selectButtonClassName={classes.flagButton}
              customLabels={CountryData}
              onSelect={(code) => setSelectedCountryCode(code)}
            />
          </div>
          <InputBase
            type="number"
            required
            placeholder="Masukkan Nomor Telepon"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("phone", e.target.value)}
            value={data.phone}
            error={data.phone?.length < 5 ? true : false}
            helperText={data.phone?.length < 5 ? "Mohon isi telepon anda" : ""}
          />
        </div>
        {data.phone.length < 5 && (
          <div style={{ margin: "10px 25px", color: "red", fontSize: 11 }}>
            Nomor Telepon tidak valid
          </div>
        )}

        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Nama Tempat
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            type="text"
            required
            placeholder="Contoh: Rumah, Kantor, dll"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("label", e.target.value)}
            value={data.label}
            error={data.label?.length <= 1 ? true : false}
            helperText={
              data.label?.length <= 1 ? "Isi bidang setidaknya 3 karakter" : ""
            }
          />
        </div>
        {data.label.length < 3 && (
          <div style={{ margin: "10px 25px", color: "red", fontSize: 11 }}>
            Isi bidang setidaknya 3 karakter
          </div>
        )}
        <div
          style={{ height: 8, backgroundColor: "#FAFAFA", marginBottom: 16 }}
        ></div>
        <div style={{ padding: "0px 16px", marginBottom: 24 }}>
          <Typography
            style={{ fontWeight: 500, fontSize: 12, marginBottom: 8 }}
          >
            Provinsi
          </Typography>
          <TextField
            required
            variant="outlined"
            fullWidth
            disabled
            value={selectedProvince || data.province}
            onChange={(e) => changeData("province", e.target.value)}
            placeholder="Pilih Provinsi"
            style={{ backgroundColor: "#FAFAFA", borderRadius: 8, height: 45 }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <ArrowDown onClick={() => handleClickOpen("provinsi")} />
                </InputAdornment>
              ),
              style: { cursor: "pointer" },
              classes: {
                notchedOutline: classes.noBorder,
                input: classes.input,
              },
            }}
            onClick={() => handleClickOpen("provinsi")}
            // eslint-disable-next-line
            inputProps={{
              style: {
                cursor: "pointer",
                color: "#333333",
                fontSize: 12,
                fontWeight: 500,
              },
            }}
          />
          {data.provinceId.length <= 1 && (
            <div style={{ margin: "10px 10px", color: "red", fontSize: 11 }}>
              Bidang ini wajib diisi!
            </div>
          )}
        </div>
        <div style={{ padding: "0px 16px", marginBottom: 16 }}>
          <Typography
            style={{ fontWeight: 500, fontSize: 12, marginBottom: 8 }}
          >
            Kota
          </Typography>
          <TextField
            variant="outlined"
            required
            fullWidth
            disabled
            value={selectedCity}
            onChange={(e) => changeData("city", e.target.value)}
            placeholder="Pilih Kota/Kabupaten"
            style={{ backgroundColor: "#FAFAFA", borderRadius: 8, height: 45 }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <ArrowDown onClick={() => handleClickOpen("kota")} />
                </InputAdornment>
              ),
              style: { cursor: "pointer" },
              classes: {
                notchedOutline: classes.noBorder,
                input: classes.input,
              },
            }}
            onClick={() => handleClickOpen("kota")}
            // eslint-disable-next-line
            inputProps={{
              style: {
                cursor: "pointer",
                color: "#333333",
                fontSize: 12,
                fontWeight: 500,
              },
            }}
          />
          {data.cityId.length <= 1 && (
            <div style={{ margin: "10px 10px ", color: "red", fontSize: 11 }}>
              Bagian ini wajib diisi!
            </div>
          )}
        </div>

        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Kode pos
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            type="number"
            required
            placeholder="Masukkan Kode Pos"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("postcode", e.target.value)}
            value={data.postcode}
          />
        </div>
        {data.postcode.length !== 5 && (
          <div style={{ margin: "10px 25px", color: "red", fontSize: 11 }}>
            Kode pos tidak valid
          </div>
        )}
        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Detail alamat
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            type="text"
            required
            placeholder="Nama Jalan, Gedung, No.Rumah"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("address", e.target.value)}
            value={data.address}
          />
        </div>
        {data.address.length < 3 && (
          <div style={{ margin: "10px 25px", color: "red", fontSize: 11 }}>
            Isi bidang setidaknya 3 karakter
          </div>
        )}
        <div style={{ fontSize: 12, fontWeight: 500, padding: "0px 16px" }}>
          Peta Lokasi
        </div>
        <div
          style={{
            fontSize: 12,
            fontWeight: 500,
            color: "grey",
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Opsional untuk pengiriman kurir instan
        </div>
        <div
          style={{ padding: "0px 16px" }}
          onClick={() => history.push("/new-address/detail")}
        >
          {long && lat ? (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                cursor: "pointer",
              }}
            >
              <p style={{ fontSize: 10, color: "grey", marginRight: 15 }}>
                {currentLocation?.display_name}
              </p>
              <div>
                <Button
                  onClick={() => {
                    history.push(`/new-address/detail?id=${data.id}`);
                    localStorage.setItem("temporaryData", JSON.stringify(data));
                  }}
                  color="primary"
                  variant="contained"
                  className={classes.button}
                >
                  Ubah
                </Button>
              </div>
            </div>
          ) : (
            <>
              <div
                style={{
                  height: 120,
                  borderRadius: 8,
                  backgroundColor: "#fafafa",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  border: "2px dashed grey",
                }}
              >
                Pilih Lokasi Anda
              </div>

              {data.latitude === "" ||
                (data.longitude === "" && (
                  <p>Isi bidang setidaknya 3 karakter</p>
                ))}
            </>
          )}
        </div>
        <div className={classes.navButton}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={() => saveAddress()}
            disabled={validateSave() ? false : true}
          >
            Simpan
          </Button>
        </div>
      </Paper>
    </Container>
  );
};

export default AddAddress;
