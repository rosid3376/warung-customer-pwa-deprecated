import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';

const styles = (theme) => ({
  container: {
    height: '100vh',
    display: 'flex',
    flexDirection: 'column',
    padding: 0,
    borderLeft: '1px solid #f1f1f1',
    borderRight: '1px solid #f1f1f1',
    background: process.env.REACT_APP_BG_SPLASH,
  },
  logoImage: {
    width: 130,
    margin: 16,
  },
  splashImage: {
    width: '100%',
    height: '100%',
  },
  progress: {
    color: '#FFFFFF',
  },
});
function SplashScreen(props) {
  const { classes } = props;
  return (
    <Container maxWidth="xs" className={classes.container}>
      <CssBaseline />
      <Grid>
        <img
          alt="{process.env.REACT_APP_BRAND_NAME}"
          src={process.env.REACT_APP_LOGO_SPLASH}
          className={classes.logoImage}
        />
      </Grid>
      <Grid
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundImage: `url(${process.env.REACT_APP_VECTOR_SPLASH})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          height: '100%',
          width: '100%',
        }}
      >
        <CircularProgress className={classes.progress} />
      </Grid>
    </Container>
  );
}

export default withStyles(styles)(SplashScreen);
