/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router-dom";

import { Button, Container, CssBaseline, Typography } from "@material-ui/core";

import AppBar from "../../components/app-bar";
import Loading from "../../components/loading";
import { getUserAddrres, setDefaultUserAddrres } from "../../services/address";
import { CartContext } from "../../context/cart";

function Component(props) {
  const { classes } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [addresses, setAddresses] = useState([]);
  const user = JSON.parse(localStorage.getItem("users"));
  const history = useHistory();
  console.log(addresses);
  const initializeApp = async () => {
    setIsLoading(true);
    const listAddress = await getUserAddrres();
    console.log(listAddress?.data);
    setAddresses(listAddress?.data);
    setIsLoading(false);
    console.log(addresses);
    if (listAddress.data?.length < 1) {
      history.push("/new-address");
    } else {
      history.push("/cart-shipment/address");
    }
  };

  const handleSetDefaultAddress = (id) => {
    setIsLoading(true);
    setDefaultUserAddrres(id)
      .then((res) => {
        console.log(res);
        initializeApp();
      })
      .catch((error) => {
        console.log(error);
        setIsLoading(false);
        alert("Gagal set-default address");
      });
  };

  const handleSetAddress = (data) => {
    localStorage.setItem("selectedAddress", JSON.stringify(data));
    props.history.push("/cart-shipment?tabs=1");
  };

  const handleUpdate = (id) => {
    history.push(`/cart-shipment/update-address?id=${id}`);
  };
  useEffect(() => {
    initializeApp();
    localStorage.removeItem("temporaryData");
    localStorage.removeItem("temp_data_address");
    if (!user) {
      history.push("/cart-shipment?tabs=0");
    }
  }, []);
  console.log(addresses);

  return (
    <React.Fragment>
      <Container
        elevation={0}
        component="main"
        maxWidth="xs"
        className={classes.container}
      >
        <CssBaseline />
        <AppBar title="Alamat" goBack={true} />
        {isLoading ? (
          <Loading />
        ) : (
          <>
            {addresses.map((item) => (
              <div className={classes.addressCard}>
                <div style={{ display: "flex", height: "100%" }}>
                  <span
                    style={{
                      position: "inherit",
                      backgroundColor: item.default ? "#FFD101" : "white",
                      minWidth: 5,
                      height: "100%",
                      borderRadius: "8px 0px 0px 8px",
                    }}
                  >
                    &nbsp;
                  </span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      marginLeft: 5,
                      padding: 10,
                      fontWeight: "bold",
                      width: "100%",
                    }}
                  >
                    {item.default ? (
                      <div
                        style={{
                          backgroundColor: "#FFD101",
                          color: "white",
                          borderRadius: 8,
                          padding: 7,
                          width: "fit-content",
                          fontSize: 12,
                        }}
                      >
                        Utama
                      </div>
                    ) : (
                      <div
                        onClick={() => handleSetDefaultAddress(item.id)}
                        style={{
                          backgroundColor: "white",
                          border: "1px solid #FFD101",
                          color: "#FFD101",
                          borderRadius: 8,
                          padding: 7,
                          width: "fit-content",
                          fontSize: 12,
                          cursor: "pointer",
                        }}
                      >
                        Jadikan Alamat Utama
                      </div>
                    )}
                    <div onClick={() => handleSetAddress(item)}>
                      <Typography
                        style={{ marginTop: 10 }}
                        variant="subtitle2"
                        display="block"
                      >
                        {item.label}
                      </Typography>
                      <Typography
                        style={{
                          whiteSpace: "nowrap",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          marginTop: 10,
                        }}
                        variant="caption"
                        display="block"
                      >
                        {item.address}
                      </Typography>
                      <Typography
                        style={{ marginTop: 5 }}
                        variant="caption"
                        display="block"
                      >
                        {item.name}
                      </Typography>
                      <Typography
                        style={{ marginTop: 5 }}
                        variant="caption"
                        display="block"
                      >
                        +62 {item.phone?.slice(2, item?.phone?.length)}
                      </Typography>
                    </div>
                    <div
                      style={{
                        color: "#FFD101",
                        fontWeight: 600,
                        cursor: "pointer",
                        marginTop: 20,
                      }}
                      onClick={() => {
                        handleUpdate(item.id);
                      }}
                    >
                      Ubah Alamat
                    </div>
                  </div>
                </div>
              </div>
            ))}
            {addresses.length < 1 && (
              <>
                <p style="center">
                  Alamat kosong silahkan tambahkan alamat terlebih dahulu
                </p>
              </>
            )}
          </>
        )}
        <div
          style={{
            position: "fixed",
            bottom: 10,
            maxWidth: 444,
            width: "100%",
            padding: "10px 16px",
          }}
        >
          <Button
            variant="contained"
            color="primary"
            style={{
              backgroundColor: "#FFD101",
              textTransform: "capitalize",
              width: "100%",
            }}
            onClick={() => history.push("/new-address")}
          >
            tambah alamat
          </Button>
        </div>
      </Container>
    </React.Fragment>
  );
}
export default Component;
