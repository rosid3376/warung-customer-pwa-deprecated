/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState, useContext } from "react";
import ContentLoader from "react-content-loader";
import Spotlight from "react-spotlight";
import axios from "axios";
import { Container, CssBaseline, Grid, Typography } from "@material-ui/core";
import Header from "../../components/header-home";
import Carousel from "../../components/slider-home";
import Category from "../../components/product-category";
import Slider from "../../components/slider-top-seller";
import FabCart from "../../components/fab";
import Fade from "@material-ui/core/Fade";

import { CartContext } from "../../context/cart";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import atas from "../../vector/atas.svg";
import Other from "../../vector/other.js";

import {
  getProductTopSellers,
  getProductCategories,
} from "../../services/products";

import firebase from "firebase/app";
import "firebase/analytics";

const MyLoader = () => (
  <ContentLoader
    height={600}
    width={400}
    speed={1}
    primaryColor="#ededed"
    secondaryColor="#d1d1d1"
  >
    <circle cx="493" cy="62" r="30" />
    <rect x="16" y="20" rx="0" ry="0" width="108" height="20" />
    <rect x="16" y="45" rx="0" ry="0" width="218" height="20" />
    <rect x="16" y="85" rx="20" ry="20" width="370" height="35" />

    <rect x="16" y="150" rx="0" ry="0" width="88" height="20" />
    <rect x="16" y="175" rx="0" ry="0" width="200" height="20" />
    <rect x="16" y="215" rx="0" ry="0" width="350" height="130" />

    <rect x="16" y="370" rx="10" ry="10" width="64" height="66" />
    <rect x="16" y="445" rx="0" ry="0" width="64" height="16" />

    <rect x="92" y="370" rx="10" ry="10" width="64" height="66" />
    <rect x="92" y="445" rx="0" ry="0" width="64" height="16" />

    <rect x="167" y="370" rx="10" ry="10" width="64" height="66" />
    <rect x="167" y="445" rx="0" ry="0" width="64" height="16" />

    <rect x="244" y="370" rx="10" ry="10" width="64" height="66" />
    <rect x="244" y="445" rx="0" ry="0" width="64" height="16" />

    <rect x="320" y="370" rx="10" ry="10" width="64" height="66" />
    <rect x="320" y="445" rx="0" ry="0" width="64" height="16" />

    <rect x="16" y="480" rx="10" ry="10" width="64" height="66" />
    <rect x="16" y="555" rx="0" ry="0" width="64" height="16" />

    <rect x="92" y="480" rx="10" ry="10" width="64" height="66" />
    <rect x="92" y="555" rx="0" ry="0" width="64" height="16" />

    <rect x="167" y="480" rx="10" ry="10" width="64" height="66" />
    <rect x="167" y="555" rx="0" ry="0" width="64" height="16" />

    <rect x="244" y="480" rx="10" ry="10" width="64" height="66" />
    <rect x="244" y="555" rx="0" ry="0" width="64" height="16" />

    <rect x="320" y="480" rx="10" ry="10" width="64" height="66" />
    <rect x="320" y="555" rx="0" ry="0" width="64" height="16" />
  </ContentLoader>
);

function Component(props) {
  const { classes } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [isSpotlight, setIsSpotlight] = useState(true);
  const isMobile = useMediaQuery("(max-width:700px)");
  const selectedPasar = useState(
    JSON.parse(localStorage.getItem("selectedPasar"))
  );
  const [categories, setCategories] = useState([]);
  const [topSellers, setTopSellers] = useState([]);

  useEffect(() => {
    firebase.analytics().logEvent("application_started");

    setTimeout(() => {
      setIsSpotlight(false);
      localStorage.setItem("firstOpen", false);
    }, 2500);

    const getData = async (vendorId) => {
      const topSeller = await getProductTopSellers(vendorId);
      const category = await getProductCategories();

      setTopSellers(topSeller);
      setCategories(category);

      setIsLoading(false);
    };

    const vendorId = selectedPasar.id;
    getData(vendorId);
  }, []);

  const { cart } = useContext(CartContext);

  const fabStyle = () => {
    if (cart.length > 0) {
      return { marginBottom: 56 };
    }
  };

  return (
    <>
      <CssBaseline />
      {!localStorage.getItem("firstOpen") && (
        <Fade in={isSpotlight}>
          <div style={{ maxWidth: 444 }}>
            <Spotlight
              x={isMobile ? 80 : 60}
              y={10}
              color="#0A0A0D"
              radius={100}
              usePercentage
              outerStyles={{ zIndex: 9999 }}
            >
              <div style={{ marginTop: 200 }}>
                <img src={atas} alt="top" />
                <h4 style={{ color: "white", margin: "8px 0px" }}>
                  Tekan tombol Ganti
                </h4>
                <h4 style={{ color: "white", margin: 0 }}>
                  untuk pilih Lokasi
                </h4>
              </div>
            </Spotlight>
          </div>
        </Fade>
      )}
      <Container maxWidth="xs" className={classes.container} style={fabStyle()}>
        {isLoading ? (
          <MyLoader />
        ) : (
          <>
            <div
              style={{
                position: "fixed",
                maxWidth: 442,
                top: 0,

                zIndex: 99,
                height: 150,
                width: "100%",
                backgroundColor: "#fff",
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.1)",
              }}
            >
              <Header />
            </div>
            <br />
            <div className={classes.gridSlider} style={{ marginTop: 143 }}>
              <Carousel />
            </div>
            <div className={classes.gridCategories}>
              <div className={classes.gridCategory}>
                {categories.slice(0, 5).map((item) => {
                  return (
                    <div style={{ width: "20%" }}>
                      <Category
                        image={item.image.url}
                        name={item.name}
                        id={item.id}
                      />
                    </div>
                  );
                })}
              </div>
              <div className={classes.gridCategory} style={{ marginTop: 12 }}>
                {categories.slice(5, 9).map((item) => {
                  return (
                    <div style={{ width: "20%" }}>
                      <Category
                        image={item.image.url}
                        name={item.name}
                        id={item.id}
                      />
                    </div>
                  );
                })}
                {categories.length >= 9 ? (
                  <div
                    style={{ width: "20%" }}
                    onClick={() => {
                      props.history.push("/category/107");
                    }}
                  >
                    <Category
                      image={<Other />}
                      name="Lainnya"
                      id="kategori-lain"
                      other
                    />
                  </div>
                ) : (
                  <> </>
                )}
              </div>
            </div>
            {process.env.REACT_APP_TOP_SELLER === "true" ? (
              <div className={classes.gridTopSeller}>
                <Grid
                  item
                  xs={12}
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    padding: "16px 16px 20px",
                  }}
                >
                  <Grid item xs={9}>
                    <Typography style={{ fontSize: 14, fontWeight: "bold" }}>
                      Pilihan Hemat Bulan ini
                    </Typography>
                    <Typography
                      style={{
                        fontSize: 10,
                        color: "#252525",
                      }}
                    >
                      Diskon spesial untukmu yang spesial
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    xs={3}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "flex-end",
                    }}
                  >
                    <span
                      className={classes.moreText}
                      onClick={() => {
                        props.history.push("/top-seller");
                      }}
                    >
                      Lihat Semua
                    </span>
                  </Grid>
                </Grid>
                <Grid className={classes.scrollingWrapper}>
                  {topSellers.map((item) => {
                    return <Slider top={item} />;
                  })}
                  <Grid className={classes.sliderMarginEnd} />
                </Grid>
              </div>
            ) : (
              <></>
            )}
          </>
        )}
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <FabCart to="/cart?from=/" />
        </div>
      </Container>
    </>
  );
}

export default Component;
