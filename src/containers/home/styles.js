const styles = (theme) => ({
  container: {
    padding: 0,
    minHeight: '100vh',
    height: '100%',
    maxWidth: 444,
    backgroundColor: '#FAFAFA',
    borderLeft: '1px solid #f1f1f1',
    borderRight: '1px solid #f1f1f1',
    paddingBottom: 56,
  },
  gridSlider: {
    marginTop: 8,
    width: '100%',
    backgroundColor: '#fff',
  },
  gridCategories: {
    marginTop: 8,
    width: '100%',
    padding: 16,
    backgroundColor: '#fff',
  },
  gridCategory: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
  gridTopSeller: {
    minHeight: 390,
    margin: '8px 0 0',
    backgroundColor: '#fff',
    paddingBottom: 16,
  },
  scrollingWrapper: {
    padding: '8px 16px',
    display: 'flex',
    flexWrap: 'nowrap',
    overflowX: 'auto',
    '-webkit-overflow-scrolling': 'touch',
    '&::-webkit-scrollbar': {
      display: 'none',
    },
    backgroundColor: '#fff',
    height: '110%',
  },
  sliderMarginEnd: {
    flex: '0 0 auto',
    width: '1px',
  },
  moreText: {
    color: process.env.REACT_APP_COLOR_PRIMARY,
    fontSize: 12,
    fontWeight: 'bold',
    '@media (max-width:375px)': {
      fontSize: '9px !important',
    },
    cursor: 'pointer',
  },
});

export default styles;
