import React, { useState, useEffect, useCallback } from "react";
import {
  Container,
  makeStyles,
  CssBaseline,
  Paper,
  InputBase,
  Typography,
  Divider,
  AppBar,
  Toolbar,
} from "@material-ui/core";
// import AppBar from "../../components/app-bar";
import Skeleton from "@material-ui/lab/Skeleton";
import { getLocationByOpenStreetMapStreet } from "../../services/address";
import _ from "lodash";
import CloseIcon from "@material-ui/icons/Close";
import RoomIcon from "@material-ui/icons/Room";
import { useHistory, useLocation } from "react-router";
import BackButton from "@material-ui/icons/ArrowBackIos";

const useStyles = makeStyles({
  container: {
    padding: 0,
    paddingTop: 64,
    backgroundColor: "#FAFAFA",
    borderLeft: "1px solid #f1f1f1",
    borderRight: "1px solid #f1f1f1",
    maxWidth: 444,
  },
  appBar: {
    left: "auto",
    right: "auto",
  },
  searchDiv: {
    height: 45,
    borderRadius: 100,
    backgroundColor: "#FAFAFA",
    borderRadius: 8,
    padding: "12px 16px",
    display: "flex",
    alignItems: "center",
    margin: "0px 16px 24px",
  },
  inputRoot: {
    color: "inherit",
    width: "100%",
    height: "100%",
  },
  inputInput: {
    width: "100%",
    fontSize: "14px !important",
  },
  listLocation: {
    display: "flex",
    alignItems: "center",
    padding: "10px 16px",
  },
});

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function DetailAddress() {
  const classes = useStyles();
  const query = useQuery();
  const [data, setData] = useState([]);
  const [search, setSearch] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const user = JSON.parse(localStorage.getItem("users"));
  const id = query.get("id");
  const [marker, setMarker] = useState({ lat: 0, lng: 0 });

  const onSearch = async (q) => {
    if (q !== "") {
      const response = await getLocationByOpenStreetMapStreet(q);
      setData(response);
      if (response) setIsLoading(false);
    } else {
      setData([]);
    }
  };

  const delayedQuery = useCallback(
    _.debounce((q) => {
      onSearch(q);
    }, 500),
    []
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      delayedQuery(search);
    }, 500);
    return () => clearTimeout(timer);
  }, [search]);

  const onTypeSearch = (keyword) => {
    setIsLoading(true);
    setSearch(keyword);
    // delayedQuery(keyword);
  };

  const fetchLocation = () => {
    navigator.geolocation.getCurrentPosition((position) => {
      setMarker({
        lat: position.coords.latitude.toString(),
        lng: position.coords.longitude.toString(),
      });
    });
  };

  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <CssBaseline />
      {/* <AppBar title="Tambah Detail Alamat" goBack={true} /> */}
      <AppBar
        style={{ width: "100%", maxWidth: 444, backgroundColor: "white" }}
        classes={{ positionFixed: classes.appBar }}
        elevation={1}
      >
        <Toolbar>
          <BackButton
            style={{
              color: process.env.REACT_APP_COLOR_PRIMARY,
              cursor: "pointer",
              marginRight: 10,
            }}
            onClick={() =>
              user
                ? id
                  ? history.push(`/cart-shipment/update-address?id=${id}`)
                  : history.push("/new-address")
                : history.push("/cart-shipment/update-address")
            }
          />

          <strong>Tambah Detail Alamat</strong>
        </Toolbar>
      </AppBar>
      <Paper style={{ minHeight: "90vh", paddingTop: 16 }} elevation={0}>
        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Detail Alamat
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            placeholder="Nama Jalan, Gedung, No. Rumah"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => onTypeSearch(e.target.value)}
            value={search}
          />
          {search.length > 0 && <CloseIcon onClick={() => setSearch("")} />}
        </div>
        {isLoading ? (
          <>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                padding: "0px 16px",
                width: "100%",
              }}
            >
              <Skeleton
                variant="circle"
                width={40}
                height={40}
                animation="wave"
                style={{ marginRight: 15 }}
              />
              <Skeleton animation="wave" height={65} width="100%" />
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                padding: "0px 16px",
                width: "100%",
              }}
            >
              <Skeleton
                variant="circle"
                width={40}
                height={40}
                animation="wave"
                style={{ marginRight: 15 }}
              />
              <Skeleton animation="wave" height={65} width="100%" />
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                padding: "0px 16px",
                width: "100%",
              }}
            >
              <Skeleton
                variant="circle"
                width={40}
                height={40}
                animation="wave"
                style={{ marginRight: 15 }}
              />
              <Skeleton animation="wave" height={65} width="100%" />
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                padding: "0px 16px",
                width: "100%",
              }}
            >
              <Skeleton
                variant="circle"
                width={40}
                height={40}
                animation="wave"
                style={{ marginRight: 15 }}
              />
              <Skeleton animation="wave" height={65} width="100%" />
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                padding: "0px 16px",
                width: "100%",
              }}
            >
              <Skeleton
                variant="circle"
                width={40}
                height={40}
                animation="wave"
                style={{ marginRight: 15 }}
              />
              <Skeleton animation="wave" height={65} width="100%" />
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                padding: "0px 16px",
                width: "100%",
              }}
            >
              <Skeleton
                variant="circle"
                width={40}
                height={40}
                animation="wave"
                style={{ marginRight: 15 }}
              />
              <Skeleton animation="wave" height={65} width="100%" />
            </div>
          </>
        ) : (
          <>
            {data?.length !== 0 && (
              <React.Fragment>
                {data?.map((res) => (
                  <div>
                    <div
                      className={classes.listLocation}
                      onClick={() => {
                        user
                          ? id
                            ? history.push(
                                `/address/maps?lat=${res.lat}&long=${res.lon}&id=${id}`
                              )
                            : history.push(
                                `/address/maps?lat=${res.lat}&long=${res.lon}`
                              )
                          : history.push(
                              `/address/maps?lat=${res.lat}&long=${res.lon}`
                            );

                        // fetchLocation(res.lat, res.lon);

                        console.log(res);
                      }}
                    >
                      <div className={classes.locationIconBg}>
                        <RoomIcon
                          style={{
                            color: "grey",
                            borderRadius: "100%",
                            padding: 5,
                            border: "1px solid grey",
                          }}
                        />
                      </div>
                      <div style={{ marginLeft: 20 }}>
                        <Typography
                          variant="body2"
                          style={{ fontWeight: "bold" }}
                        >
                          {res.display_name.slice(
                            0,
                            res.display_name.indexOf(",")
                          )}
                        </Typography>
                        <Typography
                          variant="caption"
                          style={{ color: "#a0a4a8" }}
                        >
                          {res.display_name}
                        </Typography>
                      </div>
                    </div>
                    <Divider />
                  </div>
                ))}
              </React.Fragment>
            )}
          </>
        )}
      </Paper>
    </Container>
  );
}

export default DetailAddress;
