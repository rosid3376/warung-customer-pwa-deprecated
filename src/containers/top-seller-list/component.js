/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, useContext } from 'react';
import AppBar from '../../components/app-bar';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import ContentLoader from 'react-content-loader';
import TopSeller from '../../components/top-seller';
import Fab from '../../components/fab';
import { Divider } from '@material-ui/core';
import { getProductTopSellers } from '../../services/products';

import { CartContext } from '../../context/cart';

const MyLoader = () => (
  <ContentLoader
    height={500}
    width={400}
    speed={1}
    primaryColor="#ededed"
    secondaryColor="#d1d1d1"
  >
    <rect x="16" y="16" rx="0" ry="0" width="84" height="84" />
    <rect x="116" y="16" rx="0" ry="0" width="190" height="16" />
    <rect x="116" y="40" rx="0" ry="0" width="105" height="16" />
    <rect x="116" y="74" rx="0" ry="0" width="74" height="25" />
    <rect x="295" y="74" rx="0" ry="0" width="90" height="25" />

    <rect x="16" y="120" rx="0" ry="0" width="84" height="84" />
    <rect x="116" y="120" rx="0" ry="0" width="190" height="16" />
    <rect x="116" y="144" rx="0" ry="0" width="105" height="16" />
    <rect x="116" y="178" rx="0" ry="0" width="74" height="25" />
    <rect x="295" y="178" rx="0" ry="0" width="90" height="25" />

    <rect x="16" y="224" rx="0" ry="0" width="84" height="84" />
    <rect x="116" y="224" rx="0" ry="0" width="190" height="16" />
    <rect x="116" y="248" rx="0" ry="0" width="105" height="16" />
    <rect x="116" y="282" rx="0" ry="0" width="74" height="25" />
    <rect x="295" y="282" rx="0" ry="0" width="90" height="25" />

    <rect x="16" y="328" rx="0" ry="0" width="84" height="84" />
    <rect x="116" y="328" rx="0" ry="0" width="190" height="16" />
    <rect x="116" y="352" rx="0" ry="0" width="105" height="16" />
    <rect x="116" y="386" rx="0" ry="0" width="74" height="25" />
    <rect x="295" y="386" rx="0" ry="0" width="90" height="25" />
  </ContentLoader>
);

function ProductList(props) {
  const { cart } = useContext(CartContext);
  const [state, setState] = useState({
    topSeller: [],
    isLoading: true,
    selectedPasar: JSON.parse(localStorage.getItem('selectedPasar')),
  });

  useEffect(() => {
    const fetchData = async () => {
      const vendorId = state.selectedPasar.id;
      const topSeller = await getProductTopSellers(vendorId);

      setState({
        topSeller,
        isLoading: false,
      });
    };
    fetchData();
  }, []);

  const botSendError = (error) => {
    axios.get('https://api.muctool.de/whois').then((response) => {
      const data = response.data;
      axios.post(
        'https://api.telegram.org/bot861570655:AAHWhNqltmMKFnhQRt-QHr34BSz-4AeuFUs/sendMessage',
        {
          chat_id: -312868350,
          text: `!!! GET TOP SELLER !!! : 
  Message: ${JSON.stringify(error.message)}
  Name: ${JSON.stringify(error.name)}
  Stack: ${JSON.stringify(error.stack)}
  Url: ${JSON.stringify(error.config.url)}
  Method: ${JSON.stringify(error.config.method)}
  TransformRequest: ${JSON.stringify(error.config.transformRequest)}
  TransformResponse: ${JSON.stringify(error.config.transformResponse)}
  
  !! IDENTITY !!:
  Ip: ${JSON.stringify(data.ip)}
  Isp: ${JSON.stringify(data.isp)}
  Country: ${JSON.stringify(data.country)}
  Browser: ${navigator.userAgent}`,
        }
      );
    });
  };

  const { classes } = props;
  const fabStyle = () => {
    if (cart.length > 0) {
      return { paddingBottom: 86, background: '#fafafa' };
    }
  };

  return (
    <React.Fragment>
      <Container component="main" maxWidth="xs" className={classes.container}>
        <CssBaseline />
        <AppBar title="Produk Terlaris" goBackHome={true} select />
        <Paper elevation={0} className={classes.paper}>
          <Grid container className={classes.grid} style={fabStyle()}>
            {state.isLoading ? (
              <div style={{ width: '100%' }}>
                <MyLoader />
              </div>
            ) : (
              state.topSeller.map((top) => (
                <Grid key={top} item xs={12}>
                  <TopSeller top={top} topSeller />
                  <Divider />
                </Grid>
              ))
            )}
          </Grid>
          <div>
            <Fab to="/cart?from=/top-seller" />
          </div>
        </Paper>
      </Container>
    </React.Fragment>
  );
}

export default ProductList;
