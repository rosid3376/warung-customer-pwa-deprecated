import React, { useState } from 'react';
import AppBar from '../../components/app-bar';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import smile from '../../vector/smile.svg';
import sad from '../../vector/sad.svg';
import info from '../../vector/info.svg';
import Fab from '../../components/wa-fab';

function Component(props) {
  const helps = {
    'apa-itu': {
      pertanyaan: 'Apa itu ' + process.env.REACT_APP_BRAND_NAME + '?',
      jawaban:
        '' +
        process.env.REACT_APP_BRAND_NAME +
        ' merupakan salah satu aplikasi penyedia komoditas pangan di Indonesia yang menyediakan sarana pembelian langsung dari konsumen ke petani. Dengan hal tersebut, maka kesejahteraan petani yang berkaitan dengan harga dapat diatasi disamping terjaminnya sehat dan baiknya komoditas pangan yang menjadi nilai lebih untuk konsumen. Semua orang dapat membeli komoditas pangan yang sehat serta baik di ' +
        process.env.REACT_APP_BRAND_NAME +
        ' untuk seluruh Indonesia baik transaksi satuan maupun banyak.',
    },
    'dari-mana': {
      pertanyaan:
        'Produk ' + process.env.REACT_APP_BRAND_NAME + ' berasal darimana ya?',
      jawaban:
        '' +
        process.env.REACT_APP_BRAND_NAME +
        ' bekerjasama dengan para petani lokal di beberapa daerah, dalam prosesnya petani melakukan standar produksi yang telah kami tetapkan.',
    },
    'standar-produksi': {
      pertanyaan:
        'Apa standar produksi para petani ' +
        process.env.REACT_APP_BRAND_NAME +
        '?',
      jawaban:
        'Tanaman diproduksi dengan kadar bahan kimia yang rendah dan setelah mengalami tahapan tersebut akan disemprot dengan cairan probiotik, bakteri baik.',
    },

    'cara-login': {
      pertanyaan:
        'Bagaimana cara login akun di ' +
        process.env.REACT_APP_BRAND_NAME +
        '?    ',
      jawaban:
        'Cukup dengan mengklik fitur Profile di pojok kanan bawah layar anda. Anda dapat masuk melalui akun email anda atau Google. ',
    },
    'lupa-sandi': {
      pertanyaan: 'Bagaimana kalau lupa kata sandi?    ',
      jawaban: '',
    },

    'kota-operasional': {
      pertanyaan:
        'Seberapa cepat ' +
        process.env.REACT_APP_BRAND_NAME +
        ' mengantarkan pesanan?',
      jawaban:
        'Untuk saat ini, cluster kami hanya melayani Kota Semarang dan sekitarnya. Semoga kedepannya dapat meluas lagi.',
    },

    'ongkos-kirim': {
      pertanyaan: 'Berapa ongkos kirimnya?',
      jawaban: 'Ongkos kirim bergantung kepada, promo',
    },
    'menerima-kembalian': {
      pertanyaan: 'Bagaimana saya dapat kembalian uang?',
      jawaban:
        'Jika ada kembalian akan dikembalikan setelah belanjaan sampai di rumah anda.',
    },

    'seberapa-cepat': {
      pertanyaan:
        'Seberapa cepat ' +
        process.env.REACT_APP_BRAND_NAME +
        ' mengantarkan pesanan?',
      jawaban:
        'Batas pemesanan sampai pukul 22.00 WIB hari ini. kami akan mengantar belanjaan ke rumah anda, besok harinya mulai jam 6 pagi.',
    },
    'waktu-pengantaran': {
      pertanyaan: 'Apa yang dimaksud dengan waktu pengantaran?',
      jawaban: 'Pengantaran belanjaan dari jam 06.00-11.00 WIB',
    },
    'cek-status-pesanan': {
      pertanyaan: 'Bagaimana saya mengecek status pesanan saya?',
      jawaban:
        'Kami akan mengkonfirmasi dulu belanjaan anda sebelum mulai belanja. anda juga dapat mengecek pesanan anda di fitur Transaksi pesanan.',
    },
    'merubah-membatalkan-pesanan': {
      pertanyaan: 'Bagaimana cara merubah atau membatalkan pesanan?',
      jawaban:
        'Anda bisa kontak kami jika ada belanjaan yang mau diubah atau batal. Maksimal sebelum jam 21.00 yah.',
    },
    'pembatalan-pesanan': {
      pertanyaan: 'Apa kebijakan  terkait pembatalan pesanan?',
      jawaban:
        'Maaf, belanjaan tidak bisa dibatalkan kalau sudah anda belanjakan.',
    },
    'ganti-rugi': {
      pertanyaan:
        'Bagaimana Proses Ganti Rugi Jika Barang Rusak/Hilang Dalam Pengiriman?',
      jawaban: ' ',
    },
    'lebih-lanjut': {
      pertanyaan:
        'Saya punya pertanyaan lebih lanjut untuk ' +
        process.env.REACT_APP_BRAND_NAME +
        '',
      jawaban:
        'Silahkan kontak kami di ' +
        process.env.REACT_APP_BRAND_NAME +
        '@gmail.com atau bisa langsung kontak whatsapp ' +
        process.env.REACT_APP_WHATSAPP_NUMBER +
        '. Inshaa Allah kami siap melayani anda.',
    },
  };
  const [state, setState] = useState({
    feedback: false,
  });
  const helpId = props.match.params.id;
  const { classes } = props;

  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <CssBaseline />
      <AppBar title="Detail Bantuan" goBack={true} divider />
      <Grid item xs={12} className={classes.question}>
        <Typography className={classes.textQuestion}>
          <b>{helps[helpId].pertanyaan}</b>
        </Typography>
      </Grid>

      <Grid container spacing={0} className={classes.textContainer}>
        <Grid item xs={12}>
          <Typography className={classes.textQuestion}>
            {helps[helpId].jawaban}
          </Typography>
        </Grid>
      </Grid>
      {state.feedback === false ? (
        <Grid
          container
          spacing={0}
          className={classes.textContainerTwo}
          align="center"
        >
          <Grid item xs={12}>
            <Typography className={classes.textQuestion}>
              Apakah penjelasan ini membantu?
            </Typography>
          </Grid>

          <Grid container spacing={0} style={{ marginTop: '4%' }}>
            <Grid item xs={6} style={{ paddingLeft: '34%' }}>
              <img
                onClick={() => setState({ feedback: true })}
                src={smile}
                alt="Smile"
              />
            </Grid>
            <Grid item xs={6} style={{ paddingRight: '34%' }}>
              <img
                onClick={() => setState({ feedback: true })}
                src={sad}
                alt="Sad"
              />
            </Grid>
          </Grid>
        </Grid>
      ) : (
        <>
          <Grid
            container
            spacing={0}
            className={classes.textContainerTwo}
            align="center"
          >
            <Grid item xs={12} className={classes.bg}>
              <Typography className={classes.textQuestion}>
                Terima kasih atas masukan kamu
              </Typography>
            </Grid>
          </Grid>

          <Grid container spacing={0} className={classes.textContainerTwo}>
            <Grid item xs={12}>
              <Typography className={classes.textQuestion}>
                Masih <b>butuh bantuan</b> atau <b>punya pertanyaan lain </b>
                yang ingin ditanyakan?{' '}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={`https://api.whatsapp.com/send?phone=${process.env.REACT_APP_WHATSAPP_NUMBER}&text=Hai ${process.env.REACT_APP_BRAND_NAME}, mau tanya dong`}
                  style={{
                    marginLeft: 6,
                    color: process.env.REACT_APP_COLOR_FONT,
                    textDecoration: 'none',
                  }}
                >
                  <b>HUBUNGI KAMI</b>
                </a>
              </Typography>
            </Grid>
          </Grid>

          <div style={{ paddingTop: '3%' }}>
            <Grid style={{ padding: 5 }} container spacing={0}>
              <Grid align="center" item xs={1}>
                <img src={info} alt="Info" />
              </Grid>
              <Grid item xs={11}>
                <Typography
                  style={{ color: '#898B8C' }}
                  variant="caption"
                  display="block"
                  gutterBottom
                >
                  <b>
                    Layanan pelanggan 24 jam, Senin sampai Minggu (tidak
                    termasuk libur nasional)
                  </b>
                </Typography>
              </Grid>
            </Grid>
          </div>
        </>
      )}
      <div style={{ marginLeft: '74%' }}>
        <a
          target="_blank"
          rel="noreferrer"
          href={`https://api.whatsapp.com/send?phone=${process.env.REACT_APP_WHATSAPP_NUMBER}&text=Hai ${process.env.REACT_APP_BRAND_NAME}, mau tanya dong`}
          style={{
            color: process.env.REACT_APP_COLOR_FONT,
            textDecoration: 'none',
          }}
        >
          <Fab />
        </a>
      </div>
    </Container>
  );
}

export default Component;
