import React, { useState, useEffect } from "react";
import {
  Container,
  CssBaseline,
  Typography,
  Box,
  makeStyles,
  Button,
  withStyles,
} from "@material-ui/core";
import AppBar from "../../components/app-bar";
import CursorIcon from "../../vector/cursor-icon.svg";
import PickupIcon from "../../vector/pickup-icon.svg";
import MoreIcon from "@material-ui/icons/ExpandMoreRounded";
import { getOrderDetails, cancelOrder } from "../../services/orders";
import Skeleton from "@material-ui/lab/Skeleton";
import CurrencyFormatter from "../../utilities/currency-formatter";

const CustomPaper = (props) => {
  return (
    <Box
      padding="16px"
      marginBottom="8px"
      {...props}
      style={{ backgroundColor: "white" }}
    >
      {props.title && (
        <Box
          display="flex"
          justifyContent="space-between"
          paddingBottom="16px"
          borderBottom="1px solid #f5f5f5"
          marginBottom="16px"
        >
          <Typography style={{ fontWeight: 600, fontSize: 14 }}>
            {props.title}
          </Typography>
          {props.subTitle && (
            <Typography
              style={{ fontWeight: 600, fontSize: 12, color: "#808080" }}
            >
              {props.subTitle}
            </Typography>
          )}
        </Box>
      )}
      {props.children}
    </Box>
  );
};

const itemListClasses = makeStyles({
  title: { fontWeight: 600, fontSize: 14, marginBottom: 8 },
  note: { fontWeight: 400, fontSize: 10 },
  noteValue: { fontWeight: 600, fontSize: 10 },
  price: { fontWeight: 600, fontSize: 14, marginTop: 18 },
});

const ItemList = ({ imageUrl, name, note, price }) => {
  const classes = itemListClasses();

  return (
    <Box
      display="flex"
      paddingBottom="25px"
      borderBottom="1px solid #f5f5f5"
      marginBottom="16px"
    >
      <img
        style={{ alignSelf: "flex-start", marginRight: 16 }}
        src={imageUrl}
        width="60px"
        height="60px"
        alt="item"
      />
      <div>
        <Typography className={classes.title}>{name}</Typography>
        {/* <Typography className={classes.note}>
          Pilihan Kulit Kentang :{" "}
          <span className={classes.noteValue}>Kulit Dikupas</span>
        </Typography> */}
        <Typography className={classes.note}>
          Catatan :{" "}
          <span className={classes.noteValue}>{note ? note : "-"}</span>
        </Typography>
        <Typography className={classes.price}>
          {CurrencyFormatter.format(price)}
        </Typography>
      </div>
    </Box>
  );
};

const CustomButton = withStyles({
  root: {
    padding: "12px 0",
    marginBottom: "16px",
    borderRadius: 8,
  },
  text: {
    color: "#808080",
  },
  label: {
    textTransform: "initial",
    fontWeight: 600,
    fontSize: 14,
  },
  containedPrimary: {
    color: "white",
  },
})(Button);

const KeyValue = ({ left, right, rightHighlight, totalPayment }) => (
  <Box
    display="flex"
    justifyContent="space-between"
    marginBottom="8px"
    style={totalPayment && { borderTop: "1px dashed #f5f5f5", paddingTop: 16 }}
  >
    <Typography style={{ fontWeight: totalPayment ? 600 : 400, fontSize: 12 }}>
      {left}
    </Typography>
    <Typography
      style={{
        fontWeight: 600,
        fontSize: 12,
        whiteSpace: "pre-line",
        textAlign: "right",
        color: rightHighlight ? process.env.REACT_APP_COLOR_PRIMARY : "initial",
      }}
    >
      {right}
    </Typography>
  </Box>
);

const StyledSkeleton = (props) => (
  <Skeleton
    {...props}
    style={{ ...props.style, borderRadius: 4 }}
    variant="rect"
  />
);

const LoadingSkeleton = () => (
  <>
    <Container
      component="main"
      maxWidth="xs"
      style={{
        padding: 0,
        paddingTop: 64,
        backgroundColor: "#FAFAFA",
        borderLeft: "1px solid #f1f1f1",
        borderRight: "1px solid #f1f1f1",
      }}
    >
      <CssBaseline />
      <AppBar title="Detail Transaksi" goBack={true} />
      <CustomPaper>
        <Box display="flex">
          <StyledSkeleton width={40} height={40} />
          <Box marginLeft="16px">
            <StyledSkeleton
              style={{ marginBottom: 6 }}
              width={108}
              height={20}
            />
            <StyledSkeleton width={82} height={14} />
          </Box>
        </Box>
      </CustomPaper>
      <CustomPaper title="Detail Pemesanan">
        <Box
          display="flex"
          paddingBottom="24px"
          borderBottom="1px solid #f5f5f5"
          marginBottom="16px"
        >
          <StyledSkeleton width={60} height={60} />
          <Box marginLeft="16px">
            <StyledSkeleton
              style={{ marginBottom: 8 }}
              width={267}
              height={21}
            />
            <StyledSkeleton
              style={{ marginBottom: 4 }}
              width={177}
              height={15}
            />
            <StyledSkeleton
              style={{ marginBottom: 18 }}
              width={177}
              height={15}
            />
            <StyledSkeleton width={88} height={22} />
          </Box>
        </Box>
        <Box
          display="flex"
          paddingBottom="24px"
          borderBottom="1px solid #f5f5f5"
          marginBottom="16px"
        >
          <StyledSkeleton width={60} height={60} />
          <Box marginLeft="16px">
            <StyledSkeleton
              style={{ marginBottom: 8 }}
              width={267}
              height={21}
            />
            <StyledSkeleton
              style={{ marginBottom: 4 }}
              width={177}
              height={15}
            />
            <StyledSkeleton
              style={{ marginBottom: 18 }}
              width={177}
              height={15}
            />
            <StyledSkeleton width={88} height={22} />
          </Box>
        </Box>
        <Box display="flex" alignItems="center" justifyContent="center">
          <StyledSkeleton width={104} height={24} />
        </Box>
      </CustomPaper>
    </Container>
  </>
);

const StatusOrder = ({ status }) => (
  <div
    style={{
      backgroundColor:
        status === "PENDING"
          ? "#FF722C"
          : status === "PROCESSING"
          ? "#2DBE78"
          : status === "CANCELLED"
          ? "#EB4755"
          : "#808080",
    }}
  >
    <Typography
      style={{
        color: "white",
        fontSize: 12,
        fontWeight: 600,
        textAlign: "center",
        padding: 8,
      }}
    >
      {status === "PENDING"
        ? "Menunggu Pembayaran"
        : status === "PROCESSING"
        ? "Pesanan Dalam Proses"
        : status === "CANCELLED"
        ? "Dibatalkan"
        : "undefiend"}
    </Typography>
  </div>
);

const Order = (props) => {
  const [isMoreTwoItems, setIsMoreTwoItems] = useState(false);
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isPickup, setIsPickup] = useState(false);
  const [hideMoreItem, setHideMoreItem] = useState(false);
  const { classes } = props;

  useEffect(() => {
    const GetData = async () => {
      const response = await getOrderDetails(props.match.params.id);
      setData(response.data.data);
      setIsLoading(false);
    };

    GetData();
    // eslint-disable-next-line
  }, []);
  console.log(data);

  useEffect(() => {
    if (data?.shippings[0]?.shippingChannel?.code === "pickup")
      setIsPickup(true);
    console.log(data);
  }, [data]);

  if (isLoading) return <LoadingSkeleton />;

  return (
    <>
      <Container component="main" maxWidth="xs" className={classes.container}>
        <CssBaseline />
        <AppBar title="Detail Transaksi" goBack={true} />
        <StatusOrder status={data?.status || "-"} />

        <CustomPaper>
          <Box display="flex" justifyContent="space-between">
            <Box display="flex">
              <img src={isPickup ? PickupIcon : CursorIcon} alt="cursor-icon" />
              <Box marginLeft="16px">
                <Typography className={classes.shippingTypeText}>
                  {isPickup ? "Ambil Sendiri" : "Pesan Antar"}
                </Typography>
                <Typography className={classes.shippingIdText}>
                  {data.id}
                </Typography>
              </Box>
            </Box>
            {/* <Button
              style={{ alignSelf: "center" }}
              classes={{
                label: classes.checkStatusText,
                outlined: classes.buttonCheckStatus,
              }}
              variant="outlined"
              color="primary"
            >
              Cek Status
            </Button> */}
          </Box>
        </CustomPaper>

        {isPickup && (
          <CustomPaper>
            <Typography className={classes.marketName}>
              {data.location.name}
            </Typography>
            <Typography
              className={classes.marketLocation}
            >{`${data.location.address} ${data.location.city}`}</Typography>
            {/* <Box display="flex" marginTop="16px">
              <Button
                style={{ marginRight: 8 }}
                classes={{
                  label: classes.checkStatusText,
                  outlined: classes.buttonCheckStatus,
                }}
                variant="outlined"
                color="primary"
              >
                Petunjuk Arah
              </Button>
              <Button
                classes={{
                  label: classes.checkStatusText,
                  outlined: classes.buttonCheckStatus,
                }}
                variant="outlined"
                color="primary"
              >
                Telepon
              </Button>
            </Box> */}
          </CustomPaper>
        )}

        <CustomPaper
          title="Detail Pemesanan"
          subTitle={new Intl.DateTimeFormat("id-ID", {
            day: "numeric",
            month: "short",
            year: "numeric",
          }).format(new Date(data.createdAt))}
        >
          {data.items.map((item, index) => {
            if (!isMoreTwoItems) {
              // eslint-disable-next-line
              if (index > 1) return;
              return (
                <ItemList
                  key={index}
                  imageUrl={item.image.url}
                  name={item.name}
                  price={item.price}
                />
              );
            } else {
              return (
                <ItemList
                  key={index}
                  imageUrl={item.image.url}
                  name={item.name}
                  price={item.price}
                />
              );
            }
          })}
          {data.items.length > 2 && !hideMoreItem && (
            <Box display="flex" justifyContent="center">
              <Button
                color="primary"
                classes={{ label: classes.moreItemText }}
                endIcon={<MoreIcon />}
                onClick={() => {
                  setIsMoreTwoItems(true);
                  setHideMoreItem(true);
                }}
              >
                +{data.items.length - 2} Lainnya
              </Button>
            </Box>
          )}
        </CustomPaper>

        {!isPickup && (
          <CustomPaper title="Detail Pengiriman">
            {!isPickup && (
              <KeyValue
                left="Metode Pengiriman"
                right={`${data.shippings[0]?.shippingChannel?.name ||
                  "-"} • ${data.shippings[0]?.shippingChannel?.service.name ||
                  "-"}`}
              />
            )}
            <KeyValue
              left="No. Resi"
              right={data.shippings[0]?.trackingNumber || "-"}
            />
            <KeyValue
              left="Detail Penerima"
              right={`${data.shipping.name} 
              +${data.shipping.phone} 
              ${data.shipping.address},  
              ${data.shipping.city}, ${data.shipping.province}, ${data.shipping.postcode}`}
            />
          </CustomPaper>
        )}

        <CustomPaper title="Detail Pembayaran">
          {!isPickup && (
            <KeyValue
              left="Metode Pembayaran"
              right={data.payments[0].paymentChannel.name}
            />
          )}
          <KeyValue
            left="Total Harga"
            right={CurrencyFormatter.format(data.subTotalPrice)}
          />
          <KeyValue
            left="Diskon"
            right={CurrencyFormatter.format(data.totalDiscount)}
            rightHighlight
          />
          <KeyValue
            left="Ongkir"
            right={
              !isPickup ? CurrencyFormatter.format(data.totalShipping) : "-"
            }
          />
          <KeyValue
            left="Total Pembayaran"
            right={CurrencyFormatter.format(data.totalPrice)}
            totalPayment
          />
        </CustomPaper>

        <CustomPaper marginBottom="unset" paddingBottom="unset">
          <Box display="flex" flexDirection="column">
            {data.status === "PENDING" && (
              <CustomButton
                onClick={() => (window.location = data.payments[0].url)}
                color="primary"
                variant="contained"
                disableElevation={true}
              >
                Bayar Sekarang
              </CustomButton>
            )}
            {data.status === "CANCELLED" && (
              <CustomButton
                onClick={() => props.history.push("/")}
                color="primary"
                variant="contained"
                disableElevation={true}
              >
                Beli Lagi
              </CustomButton>
            )}
            <CustomButton
              onClick={() => props.history.push("/help")}
              color="primary"
              variant="outlined"
            >
              Bantuan
            </CustomButton>
            {data.status !== "CANCELLED" && (
              <CustomButton
                onClick={async () => {
                  const response = await cancelOrder(data.id);
                  if (response.data) window.location.reload();
                }}
                variant="text"
              >
                Batalkan Pesanan
              </CustomButton>
            )}
          </Box>
        </CustomPaper>
      </Container>
    </>
  );
};

export default Order;
