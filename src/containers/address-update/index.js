import React, { useState, useEffect } from "react";
import {
  Container,
  makeStyles,
  CssBaseline,
  TextField,
  Paper,
  withStyles,
  Typography,
  InputAdornment,
  InputBase,
  Button,
  Dialog,
  Divider,
  AppBar,
  Toolbar,
} from "@material-ui/core";
// import AppBar from "../../components/app-bar";
import Skeleton from "@material-ui/lab/Skeleton";
import ReactFlagsSelect from "react-flags-select";
import CountryData from "../../utilities/country-code";
import BackButton from "@material-ui/icons/ArrowBackIos";
import DeleteIcon from "@material-ui/icons/Delete";
import ArrowDown from "@material-ui/icons/KeyboardArrowDown";
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";
import {
  getProvinces,
  getCities,
  searchProvince,
  searchCity,
  updateAddress,
  getAddressById,
  deleteAddress,
  getLocationByOpenStreetMapReverse,
} from "../../services/address";
import { useHistory, useLocation, useParams } from "react-router-dom";
import { queryString } from "query-string";
import { TrendingUpRounded } from "@material-ui/icons";

const useStyles = makeStyles({
  container: {
    padding: 0,
    paddingTop: 64,
    backgroundColor: "#FAFAFA",
    borderLeft: "1px solid #f1f1f1",
    borderRight: "1px solid #f1f1f1",
    paddingBottom: 64,
  },
  appBar: {
    left: "auto",
    right: "auto",
  },
  searchDiv: {
    height: 45,
    borderRadius: 100,
    backgroundColor: "#FAFAFA",
    borderRadius: 8,
    padding: "12px 16px",
    display: "flex",
    alignItems: "center",
    margin: "0px 16px 24px",
  },
  searchIcon: {
    marginRight: 10,
  },
  inputRoot: {
    marginLeft: 0,
    color: "inherit",
    width: "100%",
    height: "100%",
  },
  inputSearch: {
    marginLeft: 0,
    color: "inherit",
    width: "100%",
    height: "100%",
  },
  inputInput: {
    width: "100%",
    fontSize: "14px !important",
  },

  flagButton: {
    border: "unset",
    padding: "5px 0px",
    "&:focus": {
      outline: "unset",
    },
  },
  navButton: {
    position: "fixed",
    bottom: 0,
    width: "100%",
    maxWidth: 444,
    padding: 16,
    backgroundColor: "white",
    borderLeft: "1px solid #f1f1f1",
    borderRight: "1px solid #f1f1f1",
  },
  button: {
    textTransform: "capitalize",
    width: "100%",
    "&:hover": {
      backgroundColor: process.env.REACT_APP_COLOR_PRIMARY,
    },
  },
  noBorder: {
    border: "none",
  },
  input: {
    padding: 14,
  },
  paperFullScreen: {
    maxWidth: 444,
    height: "75vh",
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  card: {
    display: "flex",
    alignItems: "flex-end",
  },

  list: {
    margin: "70px 0px 0px",
    overflow: "auto",
  },
});

const StyledTextField = withStyles({
  root: {
    "& input": {
      backgroundColor: "#FAFAFA",
      borderRadius: 8,
      padding: "12px 16px",
    },
    "& input::placeholder": {
      fontWeight: 400,
      fontSize: 14,
      color: "#808080",
    },
    "& fieldset": {
      border: "unset",
    },
  },
})(TextField);

const CustomTextField = ({ label, placeholder }) => (
  <div style={{ marginBottom: 24, padding: "0px 16px" }}>
    <Typography style={{ fontWeight: 500, fontSize: 12, marginBottom: 8 }}>
      {label}
    </Typography>
    <StyledTextField
      fullWidth
      placeholder={placeholder}
      id="outlined-basic"
      variant="outlined"
    />
  </div>
);

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const UpdateAddress = () => {
  const classes = useStyles();
  const history = useHistory();
  const params = useParams();
  const query = useQuery();
  const lat = query.get("lat");
  const long = query.get("long");
  const user = JSON.parse(localStorage.getItem("users"));
  const address = JSON.parse(localStorage.getItem("address"));
  const [selectedCountryCode, setSelectedCountryCode] = useState("ID");
  const [province, setProvince] = useState([]);
  const [city, setCity] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [searchProvinceData, setSearchProvinceData] = useState("");
  const [searchCityData, setSearchCityData] = useState("");
  const [selectedCity, setSelectedCity] = useState("");
  const [selectedProvince, setSelectedProvince] = useState("");
  const [currentLocation, setCurrentLocation] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({
    name: "",
    email: "",
    phone: "",
    label: "",
    provinceId: "",
    cityId: "",
    postcode: "",
    address: "",
    latitude: "",
    longitude: "",
    country: "indonesia",
  });
  const [phoneCode, setPhoneCode] = useState({
    country_code: "ID",
    name: "Indonesia",
    dial_code: "+62",
  });

  let newData = new Object();
  newData.name = data.name;
  newData.email = data.email;
  newData.phone = data.phone;
  newData.label = data.label;
  newData.provinceId = data.provinceId;
  newData.cityId = data.cityId;
  newData.postcode = data.postcode;
  newData.address = data.address;
  newData.latitude = data.latitude;
  newData.longitude = data.longitude;
  newData.country = "indonesia";
  data.default ? (newData.default = true) : (newData.default = false);

  const handleClickOpen = (key) => {
    setOpen(key);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleChangeProvice = (e) => {
    setSearchProvinceData(e.target.value);
    setIsLoading(true);
  };
  const handleChangeCity = (e) => {
    setSearchCityData(e.target.value);
    setIsLoading(true);
  };

  // const [addAddress, setAddAddress] = useState(null);
  // useEffect(() => {
  //   const addAddress = localStorage.setItem("address");
  //   setAddAddress({
  //     name: addAddress?.name || "",
  //     email: addAddress?.email || "",
  //     phone: addAddress?.phone || "",
  //     place: addAddress?.place || "",
  //     provin: addAddress?.provinsi || "",
  //   });
  // });

  // const handleChange = (e) => {
  //   setAddAddress({ ...addAddress, [e.target.name]: e.target.value });
  //   localStorage.setItem(
  //     "address",
  //     JSON.stringify({ ...addAddress, [e.target.name]: e.target.value })
  //   );
  // };

  const handleSave = async () => {
    setIsLoading(true);
    const id = query.get("id");
    const response = await updateAddress(id, {
      ...newData,
      phone: (phoneCode.dial_code + data.phone).replace("+", ""),
    });
    console.log(response);
    if (response.meta.statusCode === 200) {
      setIsLoading(false);
      history.push("/cart-shipment/address");
      localStorage.removeItem("edit_data_address");
    }
  };

  const handleSaveGuest = () => {
    localStorage.setItem("address", JSON.stringify(data));
    history.push("/cart-shipment?tabs=1");
    localStorage.removeItem("edit_data_address");
    localStorage.removeItem("temporaryData");
  };

  const changeData = (key, value) => {
    setData({ ...data, [key]: value });
    console.log({ ...data, [key]: value });
  };

  const handleRemove = async () => {
    setIsLoading(true);
    const id = query.get("id");
    const response = await deleteAddress(id);
    if (response.meta.statusCode === 200) {
      setIsLoading(false);
      history.push("/cart-shipment/address");
      localStorage.removeItem("edit_data_address");
    }
  };

  const validateSave = () => {
    if (
      data.name?.length < 3 ||
      data.name?.length < 3 ||
      data.phone?.length < 5 ||
      data.label?.length < 1 ||
      data.provinceId?.length < 0 ||
      data.cityId?.length < 0 ||
      data.postcode?.length < 2 ||
      data.address?.length < 3 ||
      data?.latitude === 0 ||
      data?.longitude === 0
    ) {
      return false;
    } else {
      return true;
    }
  };

  useEffect(() => {
    const initialData = async () => {
      const id = query.get("id");
      if (localStorage.getItem("temporaryData")) {
        const temporaryData = JSON.parse(localStorage.getItem("temporaryData"));
        setData(temporaryData);
      } else {
        if (user) {
          const response = await getAddressById(id);
          if (response.meta.statusCode === 200) {
            setData({
              ...response.data,
              //   phone: removePhoneCode(response.data.phone),
            });
            console.log(response.data);
            if (!lat && !long) {
              const responseLocation = await getLocationByOpenStreetMapReverse(
                response.data.latitude,
                response.data.longitude
              );
              if (response) {
                setCurrentLocation(responseLocation);
              }
            }
          }
        } else {
          const address = JSON.parse(localStorage.getItem("address"));
          setData(address);
          console.log(data);
          if (!lat && !long) {
            const responseLocation = await getLocationByOpenStreetMapReverse(
              address.latitude,
              address.longitude
            );
            if (address) {
              setCurrentLocation(responseLocation);
            }
          }
        }
      }
    };
    initialData();

    const fetch = async () => {
      const response = await getLocationByOpenStreetMapReverse(lat, long);
      if (response) {
        setCurrentLocation(response);
      }
      console.log(response);
    };
    if (lat && long) {
      fetch();
    }
  }, []);
  useEffect(() => {
    async function fetchAPI() {
      const res = await getProvinces();
      const response = await getCities();
      setProvince(res.data);
      setCity(response.data);
    }
    fetchAPI();
    getCoordinates();
  }, []);

  const getCoordinates = () => {
    const lat = query.get("lat");
    const long = query.get("long");

    if (lat && long) {
      console.log({ ...data, latitude: lat, longitude: long });
      const localData = JSON.parse(localStorage.getItem("edit_data_address"));
      setData({ ...localData, latitude: lat, longitude: long });
      localStorage.setItem(
        "edit_data_address",
        JSON.stringify({ ...localData, latitude: lat, longitude: long })
      );
    }
  };

  useEffect(() => {
    localStorage.setItem("edit_data_address", JSON.stringify(data));
  }, [JSON.stringify(data)]);

  // useEffect(() => {
  //   const fetch = async () => {
  //     if (localStorage.getItem("temporaryData")) {
  //       const temporaryData = JSON.parse(localStorage.getItem("temporaryData"));
  //       setData(temporaryData);
  //     } else {
  //       const id = query.get("id");
  //       const response = await getAddressById(id);
  //       setData({ ...response.data });
  //     }
  //   };
  //   fetch();
  // }, []);
  console.log(data);
  useEffect(() => {
    const searchAPI = async () => {
      const res = await searchProvince(searchProvinceData);
      const response = await searchCity(searchCityData);
      setProvince(res.data);
      setCity(response.data);
      if (res.meta.statusCode === 200 || response.meta.statusCode === 200) {
        setIsLoading(false);
      }
    };
    const timer = setTimeout(() => {
      searchAPI();
    }, 1000);
    return () => clearTimeout(timer);
  }, [searchCityData, searchProvinceData]);

  useEffect(() => {
    const data = JSON.parse(localStorage.getItem("temporaryData"));
    const localSelectedProvince = province.find(
      (item) => item?.id === Number(data?.provinceId)
    );
    console.log(data);
    const localSelectedCity = city.find(
      (item) => item?.id === Number(data?.cityId)
    );

    if (localSelectedProvince) {
      setSelectedProvince(localSelectedProvince.name);
    }

    if (localSelectedCity) {
      setSelectedCity(localSelectedCity.name);
    }
  }, [JSON.stringify(province), JSON.stringify(city)]);

  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <CssBaseline />
      <Dialog
        open={open}
        fullScreen
        onClose={handleClose}
        classes={{
          paperFullScreen: classes.paperFullScreen,
          container: classes.card,
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: "8px 0px 16px",
          }}
        >
          <div
            style={{
              backgroundColor: "#f5f5f5",
              height: 4,
              width: "20%",
              borderRadius: 5,
            }}
          ></div>
        </div>

        {open === "provinsi" && (
          <>
            <div
              style={{
                position: "fixed",
                width: "100%",
                maxWidth: 444,
                padding: "25px 0px 0px",
                backgroundColor: "white",
                borderRadius: 8,
              }}
            >
              <div className={classes.searchDiv}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  placeholder="Cari Provinsi"
                  classes={{
                    root: classes.inputSearch,
                    input: classes.inputInput,
                  }}
                  InputProps={{
                    "aria-label": "search",
                  }}
                  onChange={handleChangeProvice}
                  value={searchProvinceData}
                />
                {searchProvinceData.length > 0 && (
                  <CloseIcon
                    onClick={() => setSearchProvinceData("")}
                    style={{ cursor: "pointer" }}
                  />
                )}
              </div>
            </div>
            <div className={classes.list} onClick={handleClose}>
              {isLoading ? (
                <div style={{ padding: "0px 16px" }}>
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                </div>
              ) : (
                <>
                  {" "}
                  {province.map((province) => (
                    <div
                      onClick={(e) => {
                        setSelectedProvince(province.name);
                        changeData("provinceId", province.id.toString());
                      }}
                    >
                      <div style={{ padding: 16 }}>{province.name}</div>
                      <Divider />
                    </div>
                  ))}
                </>
              )}
            </div>
            {province.length < 1 && (
              <p style={{ textAlign: "center" }}>Provinsi tidak ditemukan</p>
            )}
          </>
        )}

        {open === "kota" && (
          <>
            <div
              style={{
                position: "fixed",
                width: "100%",
                maxWidth: 444,
                padding: "25px 0px 0px",
                backgroundColor: "white",
                borderRadius: 8,
              }}
            >
              <div className={classes.searchDiv}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  placeholder="Cari Kota/Kabupaten"
                  classes={{
                    root: classes.inputSearch,
                    input: classes.inputInput,
                  }}
                  InputProps={{
                    "aria-label": "search",
                  }}
                  onChange={handleChangeCity}
                  value={searchCityData}
                />
                {searchCityData.length > 0 && (
                  <CloseIcon
                    onClick={() => setSearchCityData("")}
                    style={{ cursor: "pointer" }}
                  />
                )}
              </div>
            </div>
            <div className={classes.list} onClick={handleClose}>
              {isLoading ? (
                <div style={{ padding: "0px 16px" }}>
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                  <Skeleton animation="wave" height={60} />
                </div>
              ) : (
                <>
                  {" "}
                  {city.map((city) => (
                    <div
                      onClick={(e) => {
                        setSelectedCity(city.name);
                        changeData("cityId", city.id.toString());
                      }}
                    >
                      <div style={{ padding: 16 }}>{city.name}</div>
                      <Divider />
                    </div>
                  ))}
                </>
              )}
            </div>
            {city.length < 1 && (
              <p style={{ textAlign: "center" }}>Kota tidak ditemukan</p>
            )}
          </>
        )}
      </Dialog>
      {/* <AppBar title="Ubah Alamat" goBack={true} /> */}
      <AppBar
        style={{ width: "100%", maxWidth: 444, backgroundColor: "white" }}
        classes={{ positionFixed: classes.appBar }}
        elevation={1}
      >
        <Toolbar
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <div style={{ display: "flex", alignItems: "center" }}>
            <BackButton
              style={{
                color: process.env.REACT_APP_COLOR_PRIMARY,
                cursor: "pointer",
                marginRight: 10,
              }}
              onClick={() =>
                address
                  ? history.push("/cart-shipment?tabs=1")
                  : history.push("/cart-shipment/address")
              }
            />

            <strong>Ubah Alamat</strong>
          </div>
          <DeleteIcon
            style={{
              color: process.env.REACT_APP_COLOR_PRIMARY,
              cursor: "pointer",
              display: address ? "none" : data.default ? "none" : "",
            }}
            onClick={handleRemove}
          />
        </Toolbar>
      </AppBar>
      <Paper style={{ padding: "16px 0px 25px" }}>
        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Nama Penerima
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            type="text"
            placeholder="Masukkan Nama Lengkap"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("name", e.target.value)}
            value={data.name}
          />
        </div>
        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Email
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            type="text"
            placeholder="Masukkan Email Aktif"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("email", e.target.value)}
            value={data.email}
          />
        </div>
        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Nomor Telepon
        </div>
        <div className={classes.searchDiv}>
          <div className={classes.searchIcon}>
            <ReactFlagsSelect
              selected={selectedCountryCode}
              showSelectedLabel={false}
              fullWidth={false}
              selectButtonClassName={classes.flagButton}
              customLabels={CountryData}
              onSelect={(code) => setSelectedCountryCode(code)}
            />
          </div>
          <InputBase
            type="number"
            placeholder="Masukkan Nomor Telepon"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("phone", e.target.value)}
            value={data.phone}
          />
        </div>

        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Nama Tempat
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            type="text"
            placeholder="Contoh: Rumah, Kantor, dll"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("label", e.target.value)}
            value={data.label}
          />
        </div>
        <div
          style={{ height: 8, backgroundColor: "#FAFAFA", marginBottom: 16 }}
        ></div>
        <div style={{ padding: "0px 16px", marginBottom: 24 }}>
          <Typography
            style={{ fontWeight: 500, fontSize: 12, marginBottom: 8 }}
          >
            Provinsi
          </Typography>
          <TextField
            variant="outlined"
            fullWidth
            disabled
            value={selectedProvince || data.province}
            onChange={(e) => changeData("province", e.target.value)}
            placeholder="Pilih Provinsi"
            style={{ backgroundColor: "#FAFAFA", borderRadius: 8, height: 45 }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <ArrowDown onClick={() => handleClickOpen("provinsi")} />
                </InputAdornment>
              ),
              style: { cursor: "pointer" },
              classes: {
                notchedOutline: classes.noBorder,
                input: classes.input,
              },
            }}
            onClick={() => handleClickOpen("provinsi")}
            // eslint-disable-next-line
            inputProps={{
              style: {
                cursor: "pointer",
                color: "#333333",
                fontSize: 12,
                fontWeight: 500,
              },
            }}
            // onClick={onClick}
          />
        </div>
        <div style={{ padding: "0px 16px", marginBottom: 24 }}>
          <Typography
            style={{ fontWeight: 500, fontSize: 12, marginBottom: 8 }}
          >
            Kota
          </Typography>
          <TextField
            variant="outlined"
            fullWidth
            disabled
            value={selectedCity || data.city}
            onChange={(e) => changeData("city", e.target.value)}
            placeholder="Pilih Kota/Kabupaten"
            style={{ backgroundColor: "#FAFAFA", borderRadius: 8, height: 45 }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <ArrowDown onClick={() => handleClickOpen("kota")} />
                </InputAdornment>
              ),
              style: { cursor: "pointer" },
              classes: {
                notchedOutline: classes.noBorder,
                input: classes.input,
              },
            }}
            onClick={() => handleClickOpen("kota")}
            // eslint-disable-next-line
            inputProps={{
              style: {
                cursor: "pointer",
                color: "#333333",
                fontSize: 12,
                fontWeight: 500,
              },
            }}
            // onClick={onClick}
          />
        </div>

        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Kode pos
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            type="number"
            placeholder="Masukkan Kode Pos"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("postcode", e.target.value)}
            value={data.postcode}
          />
        </div>
        <div
          style={{
            fontWeight: 500,
            fontSize: 12,
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Detail alamat
        </div>
        <div className={classes.searchDiv}>
          <InputBase
            type="text"
            placeholder="Nama Jalan, Gedung, No.Rumah"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            InputProps={{
              "aria-label": "search",
            }}
            onChange={(e) => changeData("address", e.target.value)}
            value={data.address}
          />
        </div>
        <div style={{ fontSize: 12, fontWeight: 500, padding: "0px 16px" }}>
          Peta Lokasi
        </div>
        <div
          style={{
            fontSize: 12,
            fontWeight: 500,
            color: "grey",
            marginBottom: 8,
            padding: "0px 16px",
          }}
        >
          Opsional untuk pengiriman kurir instan
        </div>
        <div
          style={{ padding: "0px 16px" }}
          onClick={() => {
            user
              ? history.push(`/new-address/detail?id=${data.id}`)
              : history.push(`/new-address/detail`);
            localStorage.setItem("temporaryData", JSON.stringify(data));
          }}
        >
          {long && lat ? (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                cursor: "pointer",
              }}
            >
              <p style={{ fontSize: 10, color: "grey", marginRight: 15 }}>
                {currentLocation?.display_name}
              </p>
              <div>
                <Button
                  onClick={() => {
                    user
                      ? history.push(`/new-address/detail?id=${data.id}`)
                      : history.push(`/new-address/detail`);
                    localStorage.setItem("temporaryData", JSON.stringify(data));
                  }}
                  color="primary"
                  variant="contained"
                  className={classes.button}
                >
                  Ubah
                </Button>
              </div>
            </div>
          ) : (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                cursor: "pointer",
              }}
            >
              <p style={{ fontSize: 10, color: "grey", marginRight: 15 }}>
                {currentLocation?.display_name}
              </p>
              <div>
                <Button
                  onClick={() => {
                    user
                      ? history.push(`/new-address/detail?id=${data.id}`)
                      : history.push(`/new-address/detail`);
                    localStorage.setItem("temporaryData", JSON.stringify(data));
                  }}
                  color="primary"
                  variant="contained"
                  className={classes.button}
                >
                  Ubah
                </Button>
              </div>
            </div>
          )}
        </div>
        <div className={classes.navButton}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={() => (user ? handleSave() : handleSaveGuest())}
            disabled={validateSave() ? false : true}
          >
            Update
          </Button>
        </div>
      </Paper>
    </Container>
  );
};

export default UpdateAddress;
