/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useContext } from "react";
import "firebase/remote-config";
import "firebase/auth";
import { Switch, withRouter, Route } from "react-router-dom";
import BottomNavigationApp from "./components/bottom-navigation";
import PrivateRoute from "./components/private-route";
import Home from "./containers/home";
import Alamat from "./containers/daftar-alamat";
// import TambahAlamat from "./containers/tambah-alamat";
import AddAddress from "./containers/address-add";
import DetailAddress from "./containers/address-detail";
import UpdateAddress from "./containers/address-update";
import Profile from "./containers/profile";
import TermOfUse from "./containers/term-of-use";
import AboutUs from "./containers/about-us";
import PrivacyPolicy from "./containers/privacy-policy";
import Login from "./containers/login";
import Orders from "./containers/orders";
import Help from "./containers/help";
import ProductDetails from "./containers/product-details";
import ProductList from "./containers/product-list";
import Cart from "./containers/cart";
import CartReview from "./containers/cart-review";
import CartShipment from "./containers/cart-shipment";
import CartSuccess from "./containers/cart-success";
import ProfileEdit from "./containers/profile-edit";
import OrderHitory from "./containers/order-history";
import OrderDetails from "./containers/order-details";
import SplashScreen from "./containers/splash-screen";
import NotFound from "./containers/not-found";
import HelpDetails from "./containers/help-details";
import ProductSearch from "./containers/product-search";
import TimeOut from "./containers/time-out";
import Market from "./containers/market";
import TopSeller from "./containers/top-seller-list";
import Voucher from "./containers/voucher";
import { CartContext } from "./context/cart";
import AddressList from "./containers/address-list";
import packageJson from "../package.json";
import { getBuildDate } from "./utils";
import withClearCache from "./clearCache";
import Maps from "./containers/maps/index";

const ClearCacheComponent = withClearCache(MainApp);

function App(props) {
  const pasar = localStorage.getItem("selectedPasar");
  const [isLoading, setIsLoading] = useState(true);
  const homeRoute = ["/", "/orders", "/profile", "/help"];
  const { restoreCart } = useContext(CartContext);

  props.history.listen((location, action) => {
    window.scrollTo(0, 0);
  });

  useEffect(() => {
    const initializeApp = async () => {
      restoreCart();
      setIsLoading(false);
    };
    initializeApp();
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  if (!pasar) {
    return <Market />;
  }

  return (
    <React.Fragment>
      <Switch>
        {/* HOME */}
        <Route exact path="/" component={Home} />
        {/* CATEGORY & PRODUCT */}
        <Route path="/category/:id" component={ProductList} />
        <Route exact path="/product/:id" component={ProductDetails} />
        <Route exact path="/product-search" component={ProductSearch} />
        <Route exact path="/cart" component={Cart} />
        <Route exact path="/cart-review" component={CartReview} />
        <Route exact path="/cart-success" component={CartSuccess} />
        <Route exact path="/cart-shipment" component={CartShipment} />
        <Route exact path="/cart-shipment/address" component={AddressList} />
        {/* <Route exact path="/checkout-review" component={CartShipmentNew} /> */}
        <Route exact path="/top-seller" component={TopSeller} />
        {/* PASAR */}
        <Route exact path="/market" component={Market} cartStateSetDefault />
        {/* PROFILE & AUTH */}
        <PrivateRoute exact path="/profile" component={Profile} />
        <Route exact path="/profile-edit" component={ProfileEdit} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/address" component={Alamat} />
        <Route exact path="/new-address" component={AddAddress} />
        <Route exact path="/new-address/detail" component={DetailAddress} />
        <Route exact path="/address/maps" component={Maps} />
        <Route
          exact
          path="/cart-shipment/update-address"
          component={UpdateAddress}
        />
        <Route exact path="/term-of-use" component={TermOfUse} />
        <Route exact path="/about-us" component={AboutUs} />
        <Route exact path="/privacy-policy" component={PrivacyPolicy} />
        {/* <Route exact path='/register' component={Register} /> */}
        {/* ORDER */}
        <PrivateRoute exact path="/orders" component={Orders} />
        <Route exact path="/order-history" component={OrderHitory} />
        <Route exact path="/order/:id" component={OrderDetails} />
        {/* HELP */}
        <Route exact path="/help" component={Help} />
        <Route exact path="/help/:id" component={HelpDetails} />
        <Route exact path="/timeout" component={TimeOut} />
        <Route exact path="/cart-shipment/voucher" component={Voucher} />
        <Route path="/" component={NotFound} />
      </Switch>
      {homeRoute.indexOf(props.location.pathname) !== -1 && (
        <BottomNavigationApp />
      )}
      <ClearCacheComponent />
    </React.Fragment>
  );
}

function MainApp(props) {
  return (
    <div className="App">
      <p>Build date: {getBuildDate(packageJson.buildDate)}</p>
    </div>
  );
}

export default withRouter(App);
