/* eslint-disable no-unused-vars */
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter } from "react-router-dom";
import firebase from "firebase/app";
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme } from "@material-ui/core/styles";
// import { init as initApm } from '@elastic/apm-rum';
import "firebase/performance";
import CartContextProvider from "./context/cart";

// var apm = initApm({
//   // Set required service name (allowed characters: a-z, A-Z, 0-9, -, _, and space)
//   serviceName:
//     process.env.REACT_APP_API_ENDPOINT || process.env.REACT_APP_BRAND_NAME,

//   // Set custom APM Server URL (default: http://localhost:8200)
//   serverUrl:
//     'https://8e7244792ae94c45bbf0e7dbc6873faf.apm.asia-southeast1.gcp.elastic-cloud.com:443',

//   // Set service version (required for sourcemap feature)
//   serviceVersion: '',

//   distributedTracingOrigins: [process.env.TRACING_ORIGINS] || [
//     process.env.REACT_APP_API_ENDPOINT,
//   ],
// });

const theme = createMuiTheme({
  typography: {
    fontFamily: process.env.REACT_APP_FONT_FAMILY,
  },
  palette: {
    primary: {
      main: process.env.REACT_APP_COLOR_PRIMARY,
    },
    background: {
      default: "white",
    },
  },
});

const firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID,
};

firebase.initializeApp(firebaseConfig);

// Initialize Performance Monitoring and get a reference to the service
const perf = firebase.performance();
firebase.analytics();

ReactDOM.render(
  <CartContextProvider>
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </BrowserRouter>
  </CartContextProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
