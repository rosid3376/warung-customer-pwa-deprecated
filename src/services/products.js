import { axiosInstance } from "../config";

export const getProductTopSellers = async (vendorId) => {
  const selectedPasar = JSON.parse(localStorage.getItem("selectedPasar"));
  const response = await axiosInstance
    .get(`/customer/ecommerce/products?isFeatured=true`, {
      headers: {
        "x-location-id": selectedPasar.id,
      },
    })
    .catch((error) => {
      localStorage.removeItem("selectedPasar");
      window.location.replace("/market");
    });
  return response.data.data;
};

export const getProductCategories = async () => {
  const response = await axiosInstance
    .get("/customer/ecommerce/products/categories")
    .catch((error) => {
      localStorage.removeItem("selectedPasar");
      window.location.replace("/market");
    });
  return response.data.data;
};

export const getProductBrands = async () => {
  const response = await axiosInstance
    .get("/products/brands")
    .catch((error) => {
      localStorage.removeItem("selectedPasar");
      window.location.replace("/market");
    });
  return response.data.data;
};

export const getProductDetail = async (productId) => {
  const response = await axiosInstance
    .get(`/customer/ecommerce/products/${productId}`)
    .catch((error) => {
      localStorage.removeItem("selectedPasar");
      window.location.replace("/market");
    });
  return response.data.data;
};

export const getProductbyCategories = async (categoryId, vendorId) => {
  const response = await axiosInstance
    .get(`/customer/ecommerce/products?category=${categoryId}`)
    .catch((error) => {
      localStorage.removeItem("selectedPasar");
      window.location.replace("/market");
    });
  return response.data.data;
};

export const getProductbyKeyword = async (keyword, vendorId) => {
  const response = await axiosInstance
    .get(`/customer/ecommerce/products?search=${keyword}`)
    .catch((error) => {
      localStorage.removeItem("selectedPasar");
      window.location.replace("/market");
    });
  return response.data.data;
};
