import { axiosInstance } from '../config';

export const getListPasar = async keyword => {
  const response = await axiosInstance.get(`/customer/ecommerce/locations?search=${keyword}`);
  return response.data.data;
};
